open module com.verntrax.nonogram.server {

    requires com.verntrax.nonogram.solver;
    requires com.verntrax.nonogram.graphsolver;
    requires com.verntrax.nonogram.commons;
    requires com.verntrax.nonogram.solver.service;

    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;
    requires spring.web;

    requires com.fasterxml.jackson.core;
    requires java.validation;
}
