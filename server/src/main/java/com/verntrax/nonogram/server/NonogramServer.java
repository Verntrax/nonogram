package com.verntrax.nonogram.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NonogramServer {

    public static void main(String[] args) {
        SpringApplication.run(NonogramServer.class, args);
    }
}
