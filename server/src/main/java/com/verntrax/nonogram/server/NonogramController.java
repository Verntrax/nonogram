package com.verntrax.nonogram.server;

import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solverservice.SolverService;
import com.verntrax.nonogram.solverservice.dto.PuzzleDTO;
import com.verntrax.nonogram.solverservice.dto.SolutionDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class NonogramController {

    private final SolverService solverService;

    public NonogramController(SolverService solverService) {
        this.solverService = solverService;
    }

    @PostMapping("/solve")
    public SolutionDTO solve(@Valid @RequestBody PuzzleDTO puzzle) throws PuzzleUnsolvableException {
        return solverService.solve(puzzle);
    }
}
