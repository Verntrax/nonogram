package com.verntrax.nonogram.server;

import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solver.exception.SolverError;
import com.verntrax.nonogram.solverservice.converter.exception.ConversionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(ConversionException.class)
    public ResponseEntity<String> handleException(ConversionException exception) {
        return new ResponseEntity<>("Unknown conversion error", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PuzzleUnsolvableException.class)
    public ResponseEntity<String> handleException(PuzzleUnsolvableException exception) {
        return new ResponseEntity<>("Puzzle unsolvable", HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(SolverError.class)
    public ResponseEntity<String> handleException(SolverError exception) {
        return new ResponseEntity<>("Unexpected solver error", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
