package com.verntrax.nonogram.server.config;

import com.verntrax.nonogram.graphsolver.GraphSolver;
import com.verntrax.nonogram.graphsolver.GraphSolverRunner;
import com.verntrax.nonogram.solver.Solver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ForkJoinPool;

@Configuration
public class SolverConfig {

    @Bean
    public ForkJoinPool solverThreadPool() {
        return new ForkJoinPool(10);
    }

    @Bean
    public GraphSolverRunner solverRunner(ForkJoinPool pool) {
        return new GraphSolverRunner(pool);
    }

    @Bean
    public Solver solver(GraphSolverRunner solverRunner) {
        return new GraphSolver(solverRunner);
    }
}
