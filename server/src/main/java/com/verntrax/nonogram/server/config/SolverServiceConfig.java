package com.verntrax.nonogram.server.config;

import com.verntrax.nonogram.solver.Solver;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;
import com.verntrax.nonogram.solverservice.SolverService;
import com.verntrax.nonogram.solverservice.converter.Converter;
import com.verntrax.nonogram.solverservice.dto.PuzzleDTO;
import com.verntrax.nonogram.solverservice.dto.SolutionDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SolverServiceConfig {

    @Bean
    public SolverService solverService(Solver solver,
                                       Converter<PuzzleDTO, Puzzle> inputConverter,
                                       Converter<Solution, SolutionDTO> outputConverter) {
        return new SolverService(solver, inputConverter, outputConverter);
    }
}
