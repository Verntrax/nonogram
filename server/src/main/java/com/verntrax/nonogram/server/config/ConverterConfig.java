package com.verntrax.nonogram.server.config;

import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;
import com.verntrax.nonogram.solver.solution.Solution;
import com.verntrax.nonogram.solverservice.converter.*;
import com.verntrax.nonogram.solverservice.dto.PuzzleDTO;
import com.verntrax.nonogram.solverservice.dto.RgbColoredBlockDTO;
import com.verntrax.nonogram.solverservice.dto.SolutionDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConverterConfig {

    @Bean
    public Converter<String, RgbColor> stringToColorConverter() {
        return new StringToRgbColorConverter();
    }

    @Bean
    public Converter<RgbColoredBlockDTO, RgbColoredBlock> blockConverter(Converter<String, RgbColor> colorConverter) {
        return new DTOToRgbColoredBlockConverter(colorConverter);
    }

    @Bean
    public Converter<PuzzleDTO, Puzzle> dtoToPuzzleConverter(
            Converter<RgbColoredBlockDTO, RgbColoredBlock> blockConverter) {
        return new DTOToPuzzleConverter(blockConverter);
    }

    @Bean
    public Converter<Color, String> colorToStringConverter() {
        return new ColorToStringConverter();
    }

    @Bean
    public Converter<Solution, SolutionDTO> solutionToDTOConverter(Converter<Color, String> colorConverter) {
        return new SolutionToDTOConverter(colorConverter);
    }
}
