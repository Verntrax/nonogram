package com.verntrax.nonogram.solver.color;

import com.verntrax.nonogram.testutils.EqualityTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class RgbColorTest implements EqualityTest<Color> {

    @ParameterizedTest
    @ValueSource(strings = { "0000000", "12345", "064g6f" })
    public void shouldThrowExceptionUponCreationFromInvalidString(String notRgb) {
        assertThatThrownBy(() -> RgbColor.fromRgbString(notRgb))
                .isExactlyInstanceOf(NumberFormatException.class);
    }

    @Test
    public void shouldConvertColorToString() {
        // given
        String rgb = "#004f8e";
        Color color = RgbColor.fromRgbString(rgb);

        // then
        assertThat(color.toString()).isEqualTo(rgb);
    }

    @Override
    public Color newTestedObject() {
        return RgbColor.fromRgbString("#abab00");
    }

    @Override
    public List<Color> differentObjects() {
        return List.of(Color.BLANK, Color.UNKNOWN, RgbColor.fromRgbString("#123456"));
    }
}
