package com.verntrax.nonogram.solver.color;

import com.verntrax.nonogram.testutils.EqualityTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UnknownColorTest implements EqualityTest<Color> {

    @Test
    public void shouldConvertColorToString() {
        assertThat(Color.UNKNOWN.toString()).isEqualTo("UNKNOWN");
    }

    @Override
    public Color newTestedObject() {
        return Color.UNKNOWN;
    }

    @Override
    public List<Color> differentObjects() {
        return List.of(Color.BLANK, RgbColor.fromRgbString("#abcdef"));
    }
}
