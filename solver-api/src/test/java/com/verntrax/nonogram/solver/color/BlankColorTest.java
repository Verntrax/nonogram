package com.verntrax.nonogram.solver.color;

import com.verntrax.nonogram.testutils.EqualityTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BlankColorTest implements EqualityTest<Color> {

    @Test
    public void shouldConvertColorToString() {
        assertThat(Color.BLANK.toString()).isEqualTo("BLANK");
    }

    @Override
    public Color newTestedObject() {
        return Color.BLANK;
    }

    @Override
    public List<Color> differentObjects() {
        return List.of(Color.UNKNOWN, RgbColor.fromRgbString("#ffffff"));
    }
}
