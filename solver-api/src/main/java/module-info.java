module com.verntrax.nonogram.solver {

    exports com.verntrax.nonogram.solver;
    exports com.verntrax.nonogram.solver.color;
    exports com.verntrax.nonogram.solver.exception;
    exports com.verntrax.nonogram.solver.puzzle;
    exports com.verntrax.nonogram.solver.solution;

    requires com.verntrax.nonogram.commons;
}
