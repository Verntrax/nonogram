package com.verntrax.nonogram.solver.color;

public interface Color {

    String COLOR_REGEX = "(?:#[0-9a-f]{6})|(?:BLANK)|(?:UNKNOWN)";

    ValidColor BLANK = BlankColor.INSTANCE;
    Color UNKNOWN = UnknownColor.INSTANCE;
}
