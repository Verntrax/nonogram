package com.verntrax.nonogram.solver.exception;

public class SolverError extends Error {

    public SolverError(Throwable cause) {
        super(cause);
    }
}
