package com.verntrax.nonogram.solver.puzzle;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.color.ValidColor;

import java.util.Objects;

public class RgbColoredBlock {

    private final int size;
    private final RgbColor color;

    public static RgbColoredBlock fromSizeAndRgbColor(int size, RgbColor color) {
        return new RgbColoredBlock(size, color);
    }

    private RgbColoredBlock(int size, RgbColor color) {
        this.size = size;
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public ValidColor getColor() {
        return color;
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof RgbColoredBlock)) {
            return false;
        }
        RgbColoredBlock block = (RgbColoredBlock) other;
        return size == block.size
                && color.equals(block.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, color);
    }
}
