package com.verntrax.nonogram.solver.solution;

import com.verntrax.nonogram.commons.matrix.Matrices;
import com.verntrax.nonogram.commons.matrix.Matrix;
import com.verntrax.nonogram.solver.color.Color;

public class Solution {

    private final Matrix<Color> solution;

    public Solution(Matrix<Color> solution) {
        this.solution = solution.copy();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Solution)) {
            return false;
        }
        Solution s = (Solution) other;
        return solution.equals(s.solution);
    }

    @Override
    public int hashCode() {
        return solution.hashCode();
    }

    public Matrix<Color> asMatrix() {
        return Matrices.unmodifiableMatrix(solution);
    }
}
