package com.verntrax.nonogram.solver.color;

class UnknownColor implements Color {

    static final UnknownColor INSTANCE = new UnknownColor();

    private UnknownColor() {
    }

    @Override
    public boolean equals(Object other) {
        return other == this;
    }

    @Override
    public int hashCode() {
        return -2;
    }

    @Override
    public String toString() {
        return "UNKNOWN";
    }
}
