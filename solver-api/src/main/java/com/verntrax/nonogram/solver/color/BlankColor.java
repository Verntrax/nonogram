package com.verntrax.nonogram.solver.color;

class BlankColor implements ValidColor {

    static final BlankColor INSTANCE = new BlankColor();

    private BlankColor() {
    }

    @Override
    public boolean equals(Object other) {
        return other == this;
    }

    @Override
    public int hashCode() {
        return -1;
    }

    @Override
    public String toString() {
        return "BLANK";
    }
}
