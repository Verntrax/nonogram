package com.verntrax.nonogram.solver.color;

import java.util.Objects;

public class RgbColor implements ValidColor {

    private final int red;
    private final int green;
    private final int blue;

    private RgbColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public static RgbColor fromRgbString(String rgbString) {
        validateRgbString(rgbString);

        int red = intFromHex(rgbString.substring(1, 3));
        int green = intFromHex(rgbString.substring(3, 5));
        int blue = intFromHex(rgbString.substring(5, 7));

        return new RgbColor(red, green, blue);
    }

    private static void validateRgbString(String rgbString) {
        if (!rgbString.startsWith("#") || rgbString.length() != 7) {
            throw new NumberFormatException();
        }
    }

    private static int intFromHex(String s) {
        return Integer.parseInt(s, 16);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || other.getClass() != getClass()) {
            return false;
        }
        RgbColor color = (RgbColor) other;
        return red == color.red
                && green == color.green
                && blue == color.blue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(red, green, blue);
    }

    @Override
    public String toString() {
        return "#" +
                hexFromInt(red) +
                hexFromInt(green) +
                hexFromInt(blue);
    }

    private String hexFromInt(int i) {
        String result = Integer.toHexString(i);
        if (result.length() == 2) {
            return result;
        }
        return "0" + result;
    }
}
