package com.verntrax.nonogram.solver;

import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;

public interface Solver {

    Solution solve(Puzzle puzzle) throws PuzzleUnsolvableException;
}
