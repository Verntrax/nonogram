package com.verntrax.nonogram.solver.puzzle;

import com.verntrax.nonogram.commons.builder.BaseBuilder;
import com.verntrax.nonogram.solver.exception.InvalidPuzzleException;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class Puzzle {

    private final List<List<RgbColoredBlock>> rowBlocks = new ArrayList<>();
    private final List<List<RgbColoredBlock>> columnBlocks = new ArrayList<>();

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends BaseBuilder<Puzzle> {

        private Builder() {
            super(new Puzzle());
        }

        public Builder addRowBlocks(List<RgbColoredBlock> blocks) {
            getCreation().rowBlocks.add(new ArrayList<>(blocks));
            return this;
        }

        public Builder addColumnBlocks(List<RgbColoredBlock> blocks) {
            getCreation().columnBlocks.add(new ArrayList<>(blocks));
            return this;
        }

        @Override
        protected void doBuild() {
            if (getCreation().getRowCount() < 1 || getCreation().getColumnCount() < 1) {
                throw new InvalidPuzzleException();
            }
        }
    }

    private Puzzle() {
    }

    public int getRowCount() {
        return rowBlocks.size();
    }

    public int getColumnCount() {
        return columnBlocks.size();
    }

    public List<RgbColoredBlock> getBlocksForRow(int index) {
        return unmodifiableList(rowBlocks.get(index));
    }

    public List<RgbColoredBlock> getBlocksForColumn(int index) {
        return unmodifiableList(columnBlocks.get(index));
    }
}
