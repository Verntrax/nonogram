package com.verntrax.nonogram.graphsolver.aggregator;

import com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock;
import com.verntrax.nonogram.solver.color.ValidColor;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock.fromPositionSizeAndColor;
import static com.verntrax.nonogram.solver.color.RgbColor.fromRgbString;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class ColoredBlockAggregatorTest {

    private static final ValidColor BLACK = fromRgbString("#000000");
    private static final ValidColor BLUE = fromRgbString("#0000ff");

    private ColoredBlockAggregator aggregator;

    @BeforeEach
    public void createAggregator() {
        aggregator = new ColoredBlockAggregator();
    }

    @ParameterizedTest
    @MethodSource("shouldContain")
    public void shouldContain(List<PositionedColoredBlock> blocksToInsert, PositionedColoredBlock blockToCheck) {
        // given
        blocksToInsert.forEach(aggregator::addBlock);

        // when
        boolean contains = aggregator.containsBlock(blockToCheck);

        // then
        assertThat(contains)
                .as("aggregator contains block")
                .isTrue();
    }

    public static Stream<Arguments> shouldContain() {
        return Stream.of(
                Arguments.of(
                        List.of(fromPositionSizeAndColor(0, 3, BLACK)),
                        fromPositionSizeAndColor(7, 0, BLUE)),
                Arguments.of(
                        List.of(fromPositionSizeAndColor(5, 4, BLACK)),
                        fromPositionSizeAndColor(6, 3, BLACK)),
                Arguments.of(
                        List.of(fromPositionSizeAndColor(1, 2, BLACK), fromPositionSizeAndColor(3, 3, BLACK)),
                        fromPositionSizeAndColor(2, 4, BLACK)));
    }

    @ParameterizedTest
    @MethodSource("shouldNotContain")
    public void shouldNotContain(List<PositionedColoredBlock> blocksToInsert, PositionedColoredBlock blockToCheck) {
        // given
        blocksToInsert.forEach(aggregator::addBlock);

        // when
        boolean contains = aggregator.containsBlock(blockToCheck);

        // then
        assertThat(contains)
                .as("aggregator contains block")
                .isFalse();
    }

    public static Stream<Arguments> shouldNotContain() {
        return Stream.of(
                Arguments.of(
                        List.of(fromPositionSizeAndColor(0, 3, BLACK)),
                        fromPositionSizeAndColor(0, 3, BLUE)),
                Arguments.of(
                        List.of(fromPositionSizeAndColor(1, 2, BLACK)),
                        fromPositionSizeAndColor(1, 3, BLACK)));
    }

    @ParameterizedTest
    @MethodSource("shouldGetResultingBlocks")
    public void shouldGetResultingBlocksByColor(List<PositionedColoredBlock> blocksToInsert,
                                                List<PositionedColoredBlock> expectedBlackBlocks,
                                                List<PositionedColoredBlock> expectedBlueBlocks) {
        // given
        blocksToInsert.forEach(aggregator::addBlock);

        // when
        List<PositionedColoredBlock> actualBlackBlocks = aggregator.getBlocksWithColor(BLACK);
        List<PositionedColoredBlock> actualBlueBlocks = aggregator.getBlocksWithColor(BLUE);

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(actualBlackBlocks)
                    .as("aggregated black blocks")
                    .isEqualTo(expectedBlackBlocks);
            softly.assertThat(actualBlueBlocks)
                    .as("aggregated blue blocks")
                    .isEqualTo(expectedBlueBlocks);
        });
    }

    @ParameterizedTest
    @MethodSource("shouldGetResultingBlocks")
    public void shouldGetResultingBlocks(List<PositionedColoredBlock> blocksToInsert,
                                         List<PositionedColoredBlock> expectedBlackBlocks,
                                         List<PositionedColoredBlock> expectedBlueBlocks) {
        // given
        blocksToInsert.forEach(aggregator::addBlock);

        // when
        List<PositionedColoredBlock> actualBlocks = aggregator.getBlocks();

        // then
        List<PositionedColoredBlock> expectedBlocks = Stream.of(expectedBlackBlocks, expectedBlueBlocks)
                .flatMap(List::stream)
                .collect(toList());

        assertThat(actualBlocks)
                .as("aggregated blocks")
                .containsExactlyInAnyOrderElementsOf(expectedBlocks);
    }

    public static Stream<Arguments> shouldGetResultingBlocks() {
        return Stream.of(
                Arguments.of(
                        List.of(fromPositionSizeAndColor(1, 2, BLACK)),
                        List.of(fromPositionSizeAndColor(1, 2, BLACK)),
                        emptyList()),
                Arguments.of(
                        List.of(
                                fromPositionSizeAndColor(4, 5, BLACK),
                                fromPositionSizeAndColor(2, 5, BLUE),
                                fromPositionSizeAndColor(4, 1, BLUE),
                                fromPositionSizeAndColor(3, 3, BLACK)),
                        List.of(fromPositionSizeAndColor(3, 6, BLACK)),
                        List.of(fromPositionSizeAndColor(2, 5, BLUE))));
    }
}
