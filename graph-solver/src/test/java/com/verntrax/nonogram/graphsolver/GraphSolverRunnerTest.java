package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.commons.matrix.ArrayMatrix;
import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solver.exception.SolverError;
import com.verntrax.nonogram.solver.solution.Solution;
import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

public class GraphSolverRunnerTest {

    @Test
    public void shouldExecute() throws PuzzleUnsolvableException {
        // given
        Solution expectedResult = new Solution(new ArrayMatrix<>(1, 1, Color.BLANK));
        Callable<Solution> callable = () -> expectedResult;

        GraphSolverRunner runner = new GraphSolverRunner();
        Solution result = runner.execute(callable);

        // then
        assertThat(result)
                .as("expected result")
                .isEqualTo(expectedResult);
    }

    @Test
    public void shouldRethrowPuzzleUnsolvableException() {
        // given
        Callable<Solution> callable = () -> {
            throw new PuzzleUnsolvableException();
        };

        GraphSolverRunner runner = new GraphSolverRunner();
        ThrowingCallable action = () -> runner.execute(callable);

        // then
        assertThatThrownBy(action)
                .as("exception thrown by method")
                .isExactlyInstanceOf(PuzzleUnsolvableException.class);
    }

    @Test
    public void shouldThrowSolverErrorIfUnknownExceptionIsThrown() {
        // given
        Callable<Solution> callable = () -> {
            throw new Exception();
        };

        GraphSolverRunner runner = new GraphSolverRunner();
        ThrowingCallable action = () -> runner.execute(callable);

        // then
        assertThatThrownBy(action)
                .as("exception thrown by method")
                .isExactlyInstanceOf(SolverError.class);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldRethrowInterruptedException() throws ExecutionException, InterruptedException {
        // given
        ForkJoinTask<Solution> task = Mockito.mock(ForkJoinTask.class);
        doThrow(new InterruptedException()).when(task).get();

        ForkJoinPool pool = Mockito.mock(ForkJoinPool.class);
        doReturn(task).when(pool).submit(any(Callable.class));

        GraphSolverRunner runner = new GraphSolverRunner(pool);
        ThrowingCallable action = () -> runner.execute(() -> {
            throw new IllegalStateException(); // not invoked
        });

        // then
        assertThatThrownBy(action)
                .as("exception thrown by method")
                .isExactlyInstanceOf(RuntimeException.class)
                .hasCauseInstanceOf(InterruptedException.class);

        assertThat(Thread.interrupted())
                .as("current thread interrupted")
                .isTrue();
    }
}
