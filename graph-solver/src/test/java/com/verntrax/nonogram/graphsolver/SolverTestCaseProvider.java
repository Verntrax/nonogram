package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;

public interface SolverTestCaseProvider {

    Puzzle getPuzzle();

    Solution getSolution();
}
