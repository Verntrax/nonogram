package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.commons.matrix.ArrayMatrix;
import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;

import java.util.List;

import static com.verntrax.nonogram.solver.color.Color.BLANK;
import static com.verntrax.nonogram.solver.puzzle.RgbColoredBlock.fromSizeAndRgbColor;

public class BirdPuzzleSolverTestCaseProvider implements SolverTestCaseProvider {

    private static final RgbColor RED = RgbColor.fromRgbString("#ff0000");
    private static final RgbColor BLUE = RgbColor.fromRgbString("#0000ff");
    private static final RgbColor BLACK = RgbColor.fromRgbString("#000000");

    @Override
    public Puzzle getPuzzle() {
        return Puzzle.builder()
                // rows 1-5
                .addRowBlocks(List.of(
                    fromSizeAndRgbColor(2, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLUE),
                        fromSizeAndRgbColor(2, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, RED),
                        fromSizeAndRgbColor(4, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(5, BLACK)
                ))
                // rows 6-10
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(3, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(3, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(7, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, RED),
                        fromSizeAndRgbColor(4, BLACK)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(2, BLACK)
                ))
                // columns 1-5
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(3, BLACK)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLUE),
                        fromSizeAndRgbColor(6, BLACK)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(5, BLACK),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, RED)
                ))
                // columns 6-10
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(4, BLACK)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(4, BLACK)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(3, BLACK)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(3, BLACK)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK)
                ))
                // build
                .build();
    }

    @Override
    public Solution getSolution() {
        return new Solution(new ArrayMatrix<>(10, 10,
                // 1-5
                BLANK, BLANK, BLACK, BLACK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLACK, BLUE, BLACK, BLACK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                RED, BLACK, BLACK, BLACK, BLACK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLACK, BLACK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLACK, BLACK, BLACK, BLACK,
                BLACK, BLANK, BLANK, BLANK, BLANK,

                // 6-10
                BLANK, BLACK, BLACK, BLANK, BLACK,
                BLACK, BLACK, BLANK, BLANK, BLANK,

                BLANK, BLACK, BLACK, BLACK, BLANK,
                BLACK, BLACK, BLACK, BLANK, BLANK,

                BLANK, BLANK, BLACK, BLACK, BLACK,
                BLACK, BLACK, BLACK, BLACK, BLANK,

                BLANK, BLANK, BLANK, BLANK, RED,
                BLANK, BLACK, BLACK, BLACK, BLACK,

                BLANK, BLANK, BLANK, RED, RED,
                BLANK, BLANK, BLANK, BLACK, BLACK
        ));
    }
}
