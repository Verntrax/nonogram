package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.commons.matrix.ArrayMatrix;
import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;

import java.util.List;

import static com.verntrax.nonogram.solver.color.Color.BLANK;
import static com.verntrax.nonogram.solver.puzzle.RgbColoredBlock.fromSizeAndRgbColor;

public class BananaPuzzleSolverTestCaseProvider implements SolverTestCaseProvider {

    private static final RgbColor BLACK = RgbColor.fromRgbString("#000000");
    private static final RgbColor YELLOW = RgbColor.fromRgbString("#ffff00");

    @Override
    public Puzzle getPuzzle() {
        return Puzzle.builder()
                // rows 1-5
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(5, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK)))
                // rows 6-10
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(6, BLACK),
                        fromSizeAndRgbColor(5, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(4, BLACK),
                        fromSizeAndRgbColor(2, BLACK)))
                // rows 11-15
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(6, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                // rows 16-20
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(4, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(4, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(4, YELLOW),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(4, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                // rows 21-25
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(3, BLACK)))
                // columns 1-5
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(3, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(4, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                // columns 6-10
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(3, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(20, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(8, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(6, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                // columns 11-15
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(6, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(7, YELLOW),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(6, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(12, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(3, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(3, BLACK),
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                // columns 16-20
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(4, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(2, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, BLACK),
                        fromSizeAndRgbColor(1, YELLOW),
                        fromSizeAndRgbColor(1, BLACK)))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(5, BLACK)))
                // build
                .build();
    }

    @Override
    public Solution getSolution() {
        return new Solution(new ArrayMatrix<>(25, 20,
                // 1-5
                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLACK, BLACK, BLACK, BLACK, BLACK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLACK,
                BLACK, BLANK, BLANK, BLANK, BLACK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLACK, BLACK,
                BLANK, BLANK, BLANK, BLACK, BLACK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLACK, BLANK,
                BLANK, BLANK, BLACK, BLACK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, BLACK, BLANK,
                BLACK, BLANK, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                // 6-10
                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, BLANK, BLACK,
                BLANK, BLANK, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, BLACK, BLANK,
                BLACK, BLANK, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, BLANK, BLACK,
                BLANK, BLANK, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLACK, BLACK,
                BLACK, BLACK, BLACK, BLACK, BLANK,
                BLACK, BLACK, BLACK, BLACK, BLACK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLACK, BLACK, BLANK,
                BLANK, BLANK, BLACK, BLACK, BLACK,
                BLACK, BLANK, BLANK, BLANK, BLACK,
                BLACK, BLANK, BLANK, BLANK, BLANK,

                // 11-15
                BLANK, BLACK, BLACK, BLANK, BLANK,
                YELLOW, BLACK, BLACK, BLACK, BLACK,
                BLACK, BLACK, YELLOW, BLANK, BLANK,
                BLACK, BLACK, BLANK, BLANK, BLANK,

                BLACK, BLACK, BLANK, BLANK, YELLOW,
                BLACK, BLACK, BLACK, YELLOW, BLACK,
                YELLOW, YELLOW, BLACK, YELLOW, BLANK,
                BLANK, BLACK, BLACK, BLANK, BLANK,

                BLACK, BLANK, BLANK, YELLOW, BLACK,
                BLACK, YELLOW, BLACK, BLACK, YELLOW,
                BLACK, YELLOW, BLACK, BLACK, YELLOW,
                BLANK, BLANK, BLACK, BLACK, BLANK,

                BLANK, BLANK, YELLOW, BLACK, BLACK,
                YELLOW, BLACK, BLACK, YELLOW, BLACK,
                YELLOW, YELLOW, BLACK, YELLOW, BLACK,
                YELLOW, BLANK, BLANK, BLACK, BLACK,

                BLANK, YELLOW, BLACK, BLACK, YELLOW,
                YELLOW, BLACK, BLACK, BLACK, YELLOW,
                YELLOW, YELLOW, BLACK, YELLOW, YELLOW,
                BLACK, YELLOW, BLANK, BLANK, BLACK,

                // 16-20
                YELLOW, BLACK, BLACK, BLACK, YELLOW,
                BLACK, BLACK, BLACK, YELLOW, YELLOW,
                YELLOW, YELLOW, BLACK, BLACK, YELLOW,
                YELLOW, BLACK, YELLOW, BLANK, BLACK,

                YELLOW, BLACK, BLANK, BLACK, BLACK,
                BLACK, BLANK, BLACK, YELLOW, YELLOW,
                YELLOW, YELLOW, BLACK, BLACK, YELLOW,
                YELLOW, BLACK, BLACK, YELLOW, BLACK,

                BLACK, BLACK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, YELLOW, YELLOW,
                YELLOW, YELLOW, BLACK, BLACK, BLACK,
                YELLOW, BLACK, BLACK, BLACK, BLACK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, YELLOW, YELLOW,
                YELLOW, BLANK, BLACK, BLANK, BLACK,
                BLACK, BLACK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, YELLOW, YELLOW,
                BLANK, BLANK, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                // 21-25
                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, YELLOW, BLANK,
                YELLOW, BLANK, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, YELLOW, YELLOW,
                BLANK, YELLOW, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, YELLOW, BLANK,
                YELLOW, BLACK, BLACK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLACK, BLACK, YELLOW,
                BLACK, BLACK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLACK, BLACK,
                BLACK, BLANK, BLANK, BLANK, BLANK,
                BLANK, BLANK, BLANK, BLANK, BLANK
        ));
    }
}
