package com.verntrax.nonogram.graphsolver.sequencesolver;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.color.ValidColor;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Set;

import static com.verntrax.nonogram.solver.color.Color.BLANK;
import static org.assertj.core.api.Assertions.assertThat;

public class SequenceSolverTest {

    private static final RgbColor BLACK = RgbColor.fromRgbString("#000000");
    private static final RgbColor WHITE = RgbColor.fromRgbString("#ffffff");

    @ParameterizedTest
    @MethodSource("shouldSolveRow")
    public void shouldSolveRow(List<RgbColoredBlock> blocks,
                               List<Set<ValidColor>> constraints,
                               List<Set<ValidColor>> expected) {
        // given
        SequenceSolver solver = new SequenceSolver(blocks, constraints);

        // when
        List<Set<ValidColor>> actual = solver.solve();

        // then
        assertThat(actual)
                .as("new constraints")
                .isEqualTo(expected);
    }

    public static List<Arguments> shouldSolveRow() {
        return List.of(
                Arguments.of(
                        List.of(
                                RgbColoredBlock.fromSizeAndRgbColor(1, BLACK),
                                RgbColoredBlock.fromSizeAndRgbColor(1, BLACK)),
                        List.of(
                                Set.of(BLANK, BLACK),
                                Set.of(),
                                Set.of(BLANK, BLACK)),
                        List.of(
                                Set.of(),
                                Set.of(),
                                Set.of())),
                Arguments.of(
                        List.of(),
                        List.of(
                                Set.of(BLANK, BLACK),
                                Set.of(BLANK, WHITE),
                                Set.of(BLANK, BLACK)),
                        List.of(
                                Set.of(BLANK),
                                Set.of(BLANK),
                                Set.of(BLANK))),
                Arguments.of(
                        List.of(
                                RgbColoredBlock.fromSizeAndRgbColor(2, BLACK)),
                        List.of(
                                Set.of(BLANK),
                                Set.of(BLANK, BLACK),
                                Set.of(BLANK),
                                Set.of(BLANK, BLACK),
                                Set.of(BLANK, BLACK)),
                        List.of(
                                Set.of(BLANK),
                                Set.of(BLANK),
                                Set.of(BLANK),
                                Set.of(BLACK),
                                Set.of(BLACK))),
                Arguments.of(
                        List.of(
                                RgbColoredBlock.fromSizeAndRgbColor(3, BLACK),
                                RgbColoredBlock.fromSizeAndRgbColor(2, WHITE)),
                        List.of(
                                Set.of(BLANK, BLACK),
                                Set.of(BLACK),
                                Set.of(BLANK, BLACK),
                                Set.of(BLANK, BLACK),
                                Set.of(BLANK, BLACK, WHITE),
                                Set.of(BLANK, BLACK, WHITE),
                                Set.of(BLANK, BLACK),
                                Set.of(BLANK, BLACK, WHITE)),
                        List.of(
                                Set.of(BLANK, BLACK),
                                Set.of(BLACK),
                                Set.of(BLACK),
                                Set.of(BLANK, BLACK),
                                Set.of(WHITE),
                                Set.of(WHITE),
                                Set.of(BLANK),
                                Set.of(BLANK))),
                Arguments.of(
                    List.of(
                            RgbColoredBlock.fromSizeAndRgbColor(4, BLACK),
                            RgbColoredBlock.fromSizeAndRgbColor(2, BLACK),
                            RgbColoredBlock.fromSizeAndRgbColor(2, BLACK)),
                    List.of(
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK, BLACK)),
                    List.of(
                            Set.of(BLANK),
                            Set.of(BLANK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLACK),
                            Set.of(BLACK),
                            Set.of(BLACK),
                            Set.of(BLANK, BLACK),
                            Set.of(BLANK),
                            Set.of(BLACK),
                            Set.of(BLACK),
                            Set.of(BLANK),
                            Set.of(BLACK),
                            Set.of(BLACK),
                            Set.of(BLANK),
                            Set.of(BLANK))));
    }
}
