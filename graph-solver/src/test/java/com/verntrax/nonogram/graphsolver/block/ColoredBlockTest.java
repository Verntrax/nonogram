package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.color.ValidColor;
import com.verntrax.nonogram.testutils.EqualityTest;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ColoredBlockTest implements EqualityTest<ColoredBlock> {

    private static final ValidColor BLACK = RgbColor.fromRgbString("#000000");
    private static final ValidColor BLUE = RgbColor.fromRgbString("#0000ff");

    @Test
    public void shouldHaveCorrectSizeAndColor() {
        // given
        int size = 10;
        ValidColor color = BLACK;
        ColoredBlock block = ColoredBlock.fromSizeAndColor(size, color);

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(block.getSize())
                    .as("size")
                    .isEqualTo(size);
            softly.assertThat(block.getColor())
                    .as("color")
                    .isEqualTo(color);
        });
    }

    @Override
    public ColoredBlock newTestedObject() {
        return ColoredBlock.fromSizeAndColor(3, BLACK);
    }

    @Override
    public List<ColoredBlock> differentObjects() {
        return List.of(
                ColoredBlock.fromSizeAndColor(3, BLUE),
                ColoredBlock.fromSizeAndColor(2, BLACK),
                ColoredBlock.fromSizeAndColor(1, BLUE));
    }
}
