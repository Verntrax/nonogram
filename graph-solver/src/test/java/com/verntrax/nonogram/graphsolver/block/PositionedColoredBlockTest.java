package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.color.ValidColor;
import com.verntrax.nonogram.testutils.EqualityTest;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PositionedColoredBlockTest implements EqualityTest<PositionedColoredBlock> {

    private static final ValidColor BLACK = RgbColor.fromRgbString("#000000");
    private static final ValidColor RED = RgbColor.fromRgbString("#ff0000");

    @Test
    public void shouldHaveCorrectSizeStartEndAndColor() {
        // given
        int position = 2;
        int size = 4;
        ValidColor color = BLACK;
        PositionedColoredBlock block = PositionedColoredBlock.fromPositionSizeAndColor(position, size, color);

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(block.getSize())
                    .as("size")
                    .isEqualTo(size);
            softly.assertThat(block.getStart())
                    .as("start")
                    .isEqualTo(position);
            softly.assertThat(block.getEnd())
                    .as("end")
                    .isEqualTo(position + size);
            softly.assertThat(block.getColor())
                    .as("color")
                    .isEqualTo(color);
        });
    }

    @Test
    public void shouldCreateIdenticalBlocksWithDifferentBuilderMethods() {
        // given
        int position = 3;
        int size = 5;
        ValidColor color = BLACK;

        ColoredBlock partialBlock1 = ColoredBlock.fromSizeAndColor(size, color);
        PositionedBlock partialBlock2 = PositionedBlock.fromStartAndEnd(position, position + size);

        PositionedColoredBlock block1 = PositionedColoredBlock.fromPositionSizeAndColor(position, size, color);
        PositionedColoredBlock block2 = PositionedColoredBlock.fromPositionAndBlock(position, partialBlock1);
        PositionedColoredBlock block3 = PositionedColoredBlock.fromColorAndBlock(color, partialBlock2);

        // then
        assertThat(block1)
                .isEqualTo(block2)
                .isEqualTo(block3);
    }

    @Override
    public PositionedColoredBlock newTestedObject() {
        return PositionedColoredBlock.fromPositionSizeAndColor(1, 2, RED);
    }

    @Override
    public List<PositionedColoredBlock> differentObjects() {
        return List.of(
                PositionedColoredBlock.fromPositionSizeAndColor(1, 2, BLACK),
                PositionedColoredBlock.fromPositionSizeAndColor(1, 3, RED),
                PositionedColoredBlock.fromPositionSizeAndColor(2, 2, RED),
                PositionedColoredBlock.fromPositionSizeAndColor(3, 6, BLACK));
    }
}
