package com.verntrax.nonogram.graphsolver.flowgraph;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class LayeredFlowGraphTest {

    @Test
    public void shouldCreateFlowGraphWithSingleLayer() {
        // given
        LayeredFlowGraph<Integer, Integer> graph = LayeredFlowGraph.<Integer, Integer>builder()
                .withEdgePredicate(Objects::equals)
                .withEdgeGenerator(Integer::compareTo)
                .addToLayer(0, 4)
                .addToLayer(0, 5)
                .build();

        // when
        List<Integer> nodeData = graph.getNodeData();
        List<Integer> edgeData = graph.getEdgeData();

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(nodeData)
                    .as("node data")
                    .containsExactlyInAnyOrder(4, 5);
            softly.assertThat(edgeData)
                    .as("edge data")
                    .isEmpty();
        });
    }

    @Test
    public void shouldCreateFlowGraphWithMultipleLayers() {
        // given
        LayeredFlowGraph<Integer, Integer> graph = LayeredFlowGraph.<Integer, Integer>builder()
                .withEdgePredicate((i, j) -> i < j)
                .withEdgeGenerator((i, j) -> i * j)
                .addToLayer(0, 1)
                .addToLayer(1, 2)
                .addToLayer(1, 4)
                .addToLayer(1, 7)
                .addToLayer(2, 2)
                .addToLayer(2, 5)
                .addToLayer(2, 6)
                .addToLayer(3, 8)
                .addToLayer(3, 9)
                .build();

        // when
        List<Integer> nodeData = graph.getNodeData();
        List<Integer> edgeData = graph.getEdgeData();

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(nodeData)
                    .as("node data")
                    .containsExactlyInAnyOrder(1, 2, 4, 5, 6, 8, 9);
            softly.assertThat(edgeData)
                    .as("edge data")
                    .containsExactlyInAnyOrder(2, 4, 10, 12, 20, 24, 40, 45, 48, 54);
        });
    }

    @Test
    public void shouldThrowExceptionWhenBuildingEmptyGraph() {
        // given
        LayeredFlowGraph.Builder<Object, Object> graphBuilder = LayeredFlowGraph.builder();

        // when
        ThrowingCallable callable = graphBuilder::build;

        // then
        assertThatThrownBy(callable)
                .as("exception thrown by build")
                .isExactlyInstanceOf(IllegalStateException.class);
    }
}
