package com.verntrax.nonogram.graphsolver.optionsfinder;

import com.verntrax.nonogram.graphsolver.block.ColoredBlock;
import com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock;
import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.color.ValidColor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.verntrax.nonogram.solver.color.Color.BLANK;
import static org.assertj.core.api.Assertions.assertThat;

public class OptionsFinderTest {

    private static final ValidColor BLACK = RgbColor.fromRgbString("#000000");
    private static final ValidColor RED = RgbColor.fromRgbString("#ff0000");
    private static final ValidColor WHITE = RgbColor.fromRgbString("#ffffff");

    @ParameterizedTest
    @MethodSource("shouldGetOptionsFor")
    public void shouldGetOptionsFor(ColoredBlock block, Set<PositionedColoredBlock> expectedOptions) {
        // given
        OptionsFinder optionsFinder = optionsFinder();

        // when
        Set<PositionedColoredBlock> options = optionsFinder.optionsFor(block);

        // then
        assertThat(options).isEqualTo(expectedOptions);
    }

    public static Stream<Arguments> shouldGetOptionsFor() {
        ColoredBlock blankBlock = ColoredBlock.fromSizeAndColor(1, BLANK);
        ColoredBlock redBlock = ColoredBlock.fromSizeAndColor(2, RED);
        ColoredBlock whiteBlock = ColoredBlock.fromSizeAndColor(3, WHITE);

        return Stream.of(
                Arguments.of(
                        blankBlock,
                        Set.of(
                                PositionedColoredBlock.fromPositionAndBlock(0, blankBlock),
                                PositionedColoredBlock.fromPositionAndBlock(4, blankBlock),
                                PositionedColoredBlock.fromPositionAndBlock(5, blankBlock))),
                Arguments.of(
                        redBlock,
                        Set.of(
                                PositionedColoredBlock.fromPositionAndBlock(1, redBlock),
                                PositionedColoredBlock.fromPositionAndBlock(2, redBlock),
                                PositionedColoredBlock.fromPositionAndBlock(3, redBlock))),
                Arguments.of(
                        whiteBlock,
                        Set.of()));
    }

    @ParameterizedTest
    @MethodSource("shouldCheckIfHasOption")
    public void shouldCheckIfHasOption(PositionedColoredBlock block, boolean expectedHasOption) {
        // given
        OptionsFinder optionsFinder = optionsFinder();

        // when
        boolean hasOption = optionsFinder.hasOption(block);

        // then
        assertThat(hasOption).isEqualTo(expectedHasOption);
    }

    public static Stream<Arguments> shouldCheckIfHasOption() {
        return Stream.of(
                Arguments.of(PositionedColoredBlock.fromPositionSizeAndColor(3, 1, WHITE), true),
                Arguments.of(PositionedColoredBlock.fromPositionSizeAndColor(2, 2, WHITE), true),
                Arguments.of(PositionedColoredBlock.fromPositionSizeAndColor(0, 4, BLACK), false),
                Arguments.of(PositionedColoredBlock.fromPositionSizeAndColor(4, 2, RED), false));
    }

    private OptionsFinder optionsFinder() {
        return new OptionsFinder(List.of(
                Set.of(BLANK, BLACK),
                Set.of(BLACK, RED),
                Set.of(BLACK, RED, WHITE),
                Set.of(RED, WHITE),
                Set.of(RED, BLANK),
                Set.of(BLANK)
        ));
    }
}
