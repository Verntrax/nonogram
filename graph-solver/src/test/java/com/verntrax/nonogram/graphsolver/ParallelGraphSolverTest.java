package com.verntrax.nonogram.graphsolver;

import java.util.concurrent.ForkJoinPool;

public class ParallelGraphSolverTest extends GraphSolverTest {

    private final GraphSolverRunner runner = new GraphSolverRunner(new ForkJoinPool(10));

    @Override
    protected GraphSolverRunner solverRunner() {
        return runner;
    }
}
