package com.verntrax.nonogram.graphsolver;

public class SerialGraphSolverTest extends GraphSolverTest {

    private final GraphSolverRunner runner = new GraphSolverRunner();

    @Override
    protected GraphSolverRunner solverRunner() {
        return runner;
    }
}
