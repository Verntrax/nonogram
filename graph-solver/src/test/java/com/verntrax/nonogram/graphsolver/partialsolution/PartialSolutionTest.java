package com.verntrax.nonogram.graphsolver.partialsolution;

import com.verntrax.nonogram.commons.matrix.ArrayMatrix;
import com.verntrax.nonogram.commons.matrix.Matrix;
import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.color.ValidColor;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static com.verntrax.nonogram.solver.color.Color.BLANK;
import static com.verntrax.nonogram.solver.color.Color.UNKNOWN;
import static com.verntrax.nonogram.solver.puzzle.RgbColoredBlock.fromSizeAndRgbColor;
import static java.util.Collections.nCopies;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


public class PartialSolutionTest {

    private static final RgbColor BLACK = RgbColor.fromRgbString("#000000");
    private static final RgbColor RED = RgbColor.fromRgbString("#ff0000");

    private static final int NUMBER_OF_ROWS = 3;
    private static final int NUMBER_OF_COLUMNS = 2;

    private static final Set<ValidColor> ALL_COLORS = Set.of(BLACK, RED, BLANK);

    private PartialSolution partialSolution;

    @BeforeEach
    public void init() {
        Puzzle puzzle = Puzzle.builder()
                .addRowBlocks(List.of(fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(fromSizeAndRgbColor(2, BLACK)))
                .addRowBlocks(List.of(fromSizeAndRgbColor(1, RED)))
                .addColumnBlocks(List.of(fromSizeAndRgbColor(2, BLACK), fromSizeAndRgbColor(1, RED)))
                .addColumnBlocks(List.of(fromSizeAndRgbColor(2, BLACK)))
                .build();

        partialSolution = new PartialSolution(puzzle);
    }

    @Test
    public void shouldHaveInitializedRowsAndColumns() {
        assertEquivalentToMatrix(initialColorsMatrix());
    }

    @Test
    public void shouldBeValidAfterCreation() {
        assertValid();
    }

    @Test
    public void shouldNotBeSolvedAfterCreation() {
        assertNotSolved();
    }

    @Test
    public void shouldNotBeChangedAfterCreation() {
        assertNotChanged();
    }

    @Test
    public void shouldUpdateRowWithChange() {
        // given
        List<Set<ValidColor>> update = List.of(Set.of(BLACK, BLANK), Set.of(BLACK, RED, BLANK));

        var matrix = initialColorsMatrix();
        matrix.set(2, 0, Set.of(BLACK, BLANK));

        // when
        partialSolution.updateRow(2, update);

        // then
        assertEquivalentToMatrix(matrix);
        assertChanged();
        assertOnlySomeRowsChanged(2);
        assertOnlySomeColumnsChanged(0);
    }

    @Test
    public void shouldUpdateColumnWithChange() {
        // given
        List<Set<ValidColor>> update = List.of(Set.of(BLACK), Set.of(BLACK, RED, BLANK), Set.of(RED));

        var matrix = initialColorsMatrix();
        matrix.set(0, 0, Set.of(BLACK));
        matrix.set(2, 0, Set.of(RED));

        // when
        partialSolution.updateColumn(0, update);

        // then
        assertEquivalentToMatrix(matrix);
        assertChanged();
        assertOnlySomeRowsChanged(0, 2);
        assertOnlySomeColumnsChanged(0);
    }

    @Test
    public void shouldUpdateRowWithoutChange() {
        // given
        List<Set<ValidColor>> update = new ArrayList<>(nCopies(NUMBER_OF_COLUMNS, ALL_COLORS));

        // when
        partialSolution.updateRow(1, update);

        // then
        assertEquivalentToMatrix(initialColorsMatrix());
        assertNotChanged();
    }

    @Test
    public void shouldUpdateColumnWithoutChange() {
        // given
        List<Set<ValidColor>> update = new ArrayList<>(nCopies(NUMBER_OF_ROWS, ALL_COLORS));

        // when
        partialSolution.updateColumn(0, update);

        // then
        assertEquivalentToMatrix(initialColorsMatrix());
        assertNotChanged();
    }

    @Test
    public void shouldBeValidAfterUpdate() {
        // given
        List<Set<ValidColor>> update = List.of(Set.of(RED), Set.of(BLACK, BLANK));

        // when
        partialSolution.updateRow(0, update);

        // then
        assertValid();
    }

    @Test
    public void shouldNotBeSolvedAfterUpdate() {
        // given
        List<Set<ValidColor>> update = List.of(Set.of(BLANK), Set.of(RED), Set.of(BLACK, RED));

        // when
        partialSolution.updateColumn(1, update);

        // then
        assertNotSolved();
    }

    @Test
    public void shouldBeInvalidAfterUpdate() {
        // given
        List<Set<ValidColor>> update = List.of(Set.of(), Set.of(BLACK, BLANK));

        // when
        partialSolution.updateRow(1, update);

        // then
        assertInvalid();
    }

    @Test
    public void shouldBeSolvedAfterUpdate() {
        // given
        List<Set<ValidColor>> update1 = List.of(Set.of(BLACK), Set.of(RED), Set.of(BLANK));
        List<Set<ValidColor>> update2 = List.of(Set.of(BLANK), Set.of(BLANK), Set.of(RED));

        // when
        partialSolution.updateColumn(0, update1);
        partialSolution.updateColumn(1, update2);

        // then
        assertSolved();
    }

    @Test
    public void shouldConvertToSolution() {
        // given
        Solution expected = new Solution(new ArrayMatrix<>(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS,
                UNKNOWN, RED,
                UNKNOWN, UNKNOWN,
                BLACK, BLANK
        ));

        List<Set<ValidColor>> update1 = List.of(Set.of(BLACK, RED), Set.of(RED));
        List<Set<ValidColor>> update2 = List.of(Set.of(BLACK), Set.of(BLANK));

        // when
        partialSolution.updateRow(0, update1);
        partialSolution.updateRow(2, update2);
        Solution actual = partialSolution.toSolution();

        // then
        assertThat(actual)
                .as("solution")
                .isEqualTo(expected);
    }

    @Test
    public void shouldThrowExceptionWhenConvertingInvalidToSolution() {
        // given
        List<Set<ValidColor>> update = List.of(Set.of(), Set.of());

        // when
        partialSolution.updateRow(1, update);
        ThrowableAssert.ThrowingCallable method = () -> partialSolution.toSolution();

        // then
        assertThatThrownBy(method)
                .as("exception thrown by to solution conversion")
                .isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void shouldResetChangeOnRow() {
        // given
        List<Set<ValidColor>> update = List.of(Set.of(RED), Set.of(BLANK), ALL_COLORS);

        // when
        partialSolution.updateColumn(1, update);
        partialSolution.resetChangeOnRow(0);

        // then
        assertOnlySomeRowsChanged(1); // 0, 1 without reset
        assertOnlySomeColumnsChanged(1);
    }

    @Test
    public void shouldResetChangeOnColumn() {
        // given
        List<Set<ValidColor>> update = List.of(ALL_COLORS, Set.of(BLANK));

        // when
        partialSolution.updateRow(2, update);
        partialSolution.resetChangeOnColumn(1);

        // then
        assertNoRowChanged();
        assertNoColumnChanged();
    }

    private void assertEquivalentToMatrix(Matrix<Set<ValidColor>> matrix) {
        SoftAssertions softly = new SoftAssertions();
        IntStream.range(0, NUMBER_OF_ROWS).forEach(i ->
                softly.assertThat(partialSolution.getRow(i))
                        .as("row %s", i)
                        .isEqualTo(matrix.row(i))
        );
        IntStream.range(0, NUMBER_OF_COLUMNS).forEach(i ->
                softly.assertThat(partialSolution.getColumn(i))
                        .as("column %s", i)
                        .isEqualTo(matrix.column(i))
        );
        softly.assertAll();
    }

    private void assertChanged() {
        assertChanged(true);
    }

    private void assertNotChanged() {
        assertChanged(false);
        assertNoRowChanged();
        assertNoColumnChanged();
    }

    private void assertChanged(boolean changed) {
        assertThat(partialSolution.isChanged())
                .as("partial solution changed")
                .isEqualTo(changed);
    }

    private void assertValid() {
        assertValid(true);
    }

    private void assertInvalid() {
        assertValid(false);
    }

    private void assertValid( boolean valid) {
        assertThat(partialSolution.isValid())
                .as("partial solution valid")
                .isEqualTo(valid);
    }

    private void assertSolved() {
        assertSolved(true);
    }

    private void assertNotSolved() {
        assertSolved(false);
    }

    private void assertSolved(boolean solved) {
        assertThat(partialSolution.isSolved())
                .as("partial solution solved")
                .isEqualTo(solved);
    }

    private void assertNoRowChanged() {
        assertOnlySomeRowsChanged();
    }

    private void assertNoColumnChanged() {
        assertOnlySomeColumnsChanged();
    }

    private void assertOnlySomeRowsChanged(Integer... rowIndices) {
        Set<Integer> changedRowsSet = Set.of(rowIndices);

        SoftAssertions softly = new SoftAssertions();
        IntStream.range(0, NUMBER_OF_ROWS).forEach(i ->
                softly.assertThat(partialSolution.isRowChanged(i))
                        .as("row %s changed", i)
                        .isEqualTo(changedRowsSet.contains(i))
        );
        softly.assertAll();
    }

    private void assertOnlySomeColumnsChanged(Integer... columnIndices) {
        Set<Integer> changedColumnsSet = Set.of(columnIndices);

        SoftAssertions softly = new SoftAssertions();
        IntStream.range(0, NUMBER_OF_COLUMNS).forEach(i ->
                softly.assertThat(partialSolution.isColumnChanged(i))
                        .as("column %s changed", i)
                        .isEqualTo(changedColumnsSet.contains(i))
        );
        softly.assertAll();
    }

    private Matrix<Set<ValidColor>> initialColorsMatrix() {
        Matrix<Set<ValidColor>> matrix = new ArrayMatrix<>(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS);
        matrix.populate((i, j) -> ALL_COLORS);
        return matrix;
    }
}
