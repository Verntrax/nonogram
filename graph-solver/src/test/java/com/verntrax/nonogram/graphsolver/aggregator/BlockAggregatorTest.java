package com.verntrax.nonogram.graphsolver.aggregator;

import com.verntrax.nonogram.graphsolver.block.PositionedBlock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static com.verntrax.nonogram.graphsolver.block.PositionedBlock.fromStartAndEnd;
import static org.assertj.core.api.Assertions.assertThat;

public class BlockAggregatorTest {

    private BlockAggregator aggregator;

    @BeforeEach
    public void createAggregator() {
        aggregator = new BlockAggregator();
    }

    @ParameterizedTest
    @MethodSource("shouldContain")
    public void shouldContain(List<PositionedBlock> blocksToInsert, PositionedBlock blockToCheck) {
        // given
        blocksToInsert.forEach(aggregator::addBlock);

        // when
        boolean contains = aggregator.containsBlock(blockToCheck);

        // then
        assertThat(contains)
                .as("aggregator contains block")
                .isTrue();
    }

    public static Stream<Arguments> shouldContain() {
        return Stream.of(
                Arguments.of(
                        List.of(fromStartAndEnd(1, 5), fromStartAndEnd(6, 7)),
                        fromStartAndEnd(2, 4)),
                Arguments.of(
                        List.of(fromStartAndEnd(4, 5)),
                        fromStartAndEnd(8, 8)),
                Arguments.of(
                        List.of(fromStartAndEnd(2, 6), fromStartAndEnd(6, 10)),
                        fromStartAndEnd(2, 10)));
    }

    @ParameterizedTest
    @MethodSource("shouldNotContain")
    public void shouldNotContain(List<PositionedBlock> blocksToInsert, PositionedBlock blockToCheck) {
        // given
        blocksToInsert.forEach(aggregator::addBlock);

        // when
        boolean contains = aggregator.containsBlock(blockToCheck);

        // then
        assertThat(contains)
                .as("aggregator contains block")
                .isFalse();
    }

    public static Stream<Arguments> shouldNotContain() {
        return Stream.of(
                Arguments.of(
                        List.of(fromStartAndEnd(1, 6), fromStartAndEnd(7, 8)),
                        fromStartAndEnd(5, 7)),
                Arguments.of(
                        List.of(
                                fromStartAndEnd(5, 7), fromStartAndEnd(6, 12)),
                        fromStartAndEnd(1, 4)),
                Arguments.of(
                        List.of(
                                fromStartAndEnd(2, 4), fromStartAndEnd(2, 6)),
                        fromStartAndEnd(6, 7)));
    }

    @ParameterizedTest
    @MethodSource("shouldGetResultingBlocks")
    public void shouldGetResultingBlocks(List<PositionedBlock> blocksToInsert, List<PositionedBlock> expected) {
        // given
        blocksToInsert.forEach(aggregator::addBlock);

        // when
        List<PositionedBlock> actual = aggregator.getBlocks();

        // then
        assertThat(actual)
                .as("aggregated blocks")
                .isEqualTo(expected);
    }

    public static Stream<Arguments> shouldGetResultingBlocks() {
        return Stream.of(
                Arguments.of(
                        List.of(
                                fromStartAndEnd(0, 2),
                                fromStartAndEnd(5, 8),
                                fromStartAndEnd(1, 4),
                                fromStartAndEnd(2, 2)),
                        List.of(
                                fromStartAndEnd(0, 4),
                                fromStartAndEnd(5, 8))),
                Arguments.of(
                        List.of(
                                fromStartAndEnd(0, 1),
                                fromStartAndEnd(3, 5),
                                fromStartAndEnd(3, 4),
                                fromStartAndEnd(7, 9)),
                        List.of(
                                fromStartAndEnd(0, 1),
                                fromStartAndEnd(3, 5),
                                fromStartAndEnd(7, 9))),
                Arguments.of(
                        List.of(
                                fromStartAndEnd(5, 6),
                                fromStartAndEnd(7, 9),
                                fromStartAndEnd(3, 4),
                                fromStartAndEnd(9, 10),
                                fromStartAndEnd(2, 8)),
                        List.of(
                                fromStartAndEnd(2, 10))));
    }
}
