package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.testutils.EqualityTest;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class PositionedBlockTest implements EqualityTest<PositionedBlock> {

    @Test
    public void shouldHaveCorrectSizeStartAndEnd() {
        // given
        int start = 5;
        int end = 8;
        PositionedBlock block = PositionedBlock.fromStartAndEnd(start, end);

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(block.getSize())
                    .as("size")
                    .isEqualTo(end - start);
            softly.assertThat(block.getStart())
                    .as("start")
                    .isEqualTo(start);
            softly.assertThat(block.getEnd())
                    .as("end")
                    .isEqualTo(end);
        });
    }

    @Test
    public void shouldThrowExceptionIfStartIsGreaterThanEnd() {
        // when
        ThrowableAssert.ThrowingCallable method = () -> PositionedBlock.fromStartAndEnd(5, 4);

        // then
        assertThatThrownBy(method).isExactlyInstanceOf(IllegalArgumentException.class);
    }

    @Override
    public PositionedBlock newTestedObject() {
        return PositionedBlock.fromStartAndEnd(3, 6);
    }

    @Override
    public List<PositionedBlock> differentObjects() {
        return List.of(
                PositionedBlock.fromStartAndEnd(3, 5),
                PositionedBlock.fromStartAndEnd(1, 6),
                PositionedBlock.fromStartAndEnd(2, 8));
    }
}
