package com.verntrax.nonogram.graphsolver.flowgraph;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NodeTest {

    @Test
    public void shouldGetData() {
        // given
        int data = 5;
        Node<Integer> node = new Node<>(data);

        // when
        int readData = node.getData();

        // then
        assertThat(readData).isEqualTo(data);
    }

    @Test
    public void shouldBeConnected() {
        // given
        Node<Integer> node = new Node<>(null);
        node.setConnected(true);

        // then
        assertConnected(node, true, true);
    }

    @Test
    public void shouldConnectedBeNotSet() {
        // given
        Node<Integer> node = new Node<>(null);

        // then
        assertConnected(node, false, false);
    }

    @Test
    public void shouldNotBeConnected() {
        // given
        Node<Integer> node = new Node<>(null);
        node.setConnected(false);

        // then
        assertConnected(node, false, true);
    }

    private void assertConnected(Node<?> node, boolean isConnected, boolean isConnectedSet) {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(node.isConnected())
                    .as("is node connected")
                    .isEqualTo(isConnected);
            softly.assertThat(!node.isConnectedNotSet())
                    .as("is node connected flag set")
                    .isEqualTo(isConnectedSet);
        });
    }
}
