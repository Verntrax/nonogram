package com.verntrax.nonogram.graphsolver.partialsolution;

import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.color.ValidColor;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Set;
import java.util.stream.Stream;

import static com.verntrax.nonogram.solver.color.Color.UNKNOWN;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ColorFieldTest {

    private static final ValidColor BLACK = RgbColor.fromRgbString("#000000");
    private static final ValidColor RED = RgbColor.fromRgbString("#ff0000");
    private static final ValidColor GREEN = RgbColor.fromRgbString("#00ff00");
    private static final ValidColor WHITE = RgbColor.fromRgbString("#ffffff");

    @ParameterizedTest
    @MethodSource("shouldValidate")
    public void shouldValidate(Set<ValidColor> colors, boolean expectedToBeValid) {
        // given
        ColorField field = new ColorField(colors);

        // when
        boolean isValid = field.isValid();

        // then
        assertThat(isValid)
                .as("field valid")
                .isEqualTo(expectedToBeValid);
    }

    public static Stream<Arguments> shouldValidate() {
        return Stream.of(
                Arguments.of(emptySet(), false),
                Arguments.of(singleton(BLACK), true),
                Arguments.of(Set.of(BLACK, WHITE), true));
    }

    @ParameterizedTest
    @MethodSource("shouldCheckUnequivocal")
    public void shouldCheckUnequivocal(Set<ValidColor> colors, boolean expectedToBeUnequivocal) {
        // given
        ColorField field = new ColorField(colors);

        // when
        boolean isUnequivocal = field.isUnequivocal();

        // then
        assertThat(isUnequivocal)
                .as("field unequivocal")
                .isEqualTo(expectedToBeUnequivocal);
    }

    public static Stream<Arguments> shouldCheckUnequivocal() {
        return Stream.of(
                Arguments.of(emptySet(), false),
                Arguments.of(singleton(BLACK), true),
                Arguments.of(Set.of(BLACK, WHITE), false));
    }

    @ParameterizedTest
    @MethodSource("shouldUpdate")
    public void shouldUpdate(Set<ValidColor> update, Set<ValidColor> newColors) {
        // given
        Set<ValidColor> initialColors = Set.of(BLACK, RED, WHITE);
        ColorField field = new ColorField(initialColors);

        // when
        field.update(update);

        // then
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(field.getColors())
                .as("new colors")
                .isEqualTo(newColors);
        softly.assertThat(field.isChanged())
                .as("field changed")
                .isEqualTo(!initialColors.equals(newColors));

        softly.assertAll();
    }

    public static Stream<Arguments> shouldUpdate() {
        return Stream.of(
                Arguments.of(
                        Set.of(BLACK, RED, GREEN, WHITE), Set.of(BLACK, RED, WHITE)),
                Arguments.of(
                        Set.of(BLACK, WHITE), Set.of(BLACK, WHITE)),
                Arguments.of(
                        emptySet(), emptySet()));
    }

    @Test
    public void shouldResetChanged() {
        // given
        ColorField field = new ColorField(Set.of(BLACK, WHITE));

        // when
        field.update(Set.of(BLACK));

        // then
        assertThat(field.isChanged())
                .as("field changed before reset")
                .isTrue();

        // and when
        field.resetChanged();

        // then
        assertThat(field.isChanged())
                .as("field changed after reset")
                .isFalse();
    }

    @Test
    public void shouldGetAsSingleColorFromUnequivocal() {
        // given
        ValidColor color = BLACK;
        ColorField field = new ColorField(Set.of(color));

        // when
        Color singleColor = field.asSingleColor();

        // then
        assertThat(singleColor)
                .as("single color")
                .isEqualTo(color);
    }

    @Test
    public void shouldGetAsSingleColorFromNotUnequivocal() {
        // given
        ColorField field = new ColorField(Set.of(BLACK, WHITE));

        // when
        Color singleColor = field.asSingleColor();

        // then
        assertThat(singleColor)
                .as("single color")
                .isEqualTo(UNKNOWN);
    }

    @Test
    public void shouldThrowWhenGotAsSingleColorFromInvalid() {
        // given
        ColorField field = new ColorField(emptySet());

        // when
        ThrowableAssert.ThrowingCallable method = field::asSingleColor;

        // then
        assertThatThrownBy(method).isExactlyInstanceOf(IllegalStateException.class);
    }
}
