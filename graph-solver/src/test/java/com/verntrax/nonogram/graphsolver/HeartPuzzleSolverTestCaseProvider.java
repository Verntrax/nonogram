package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.commons.matrix.ArrayMatrix;
import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;

import java.util.List;

import static com.verntrax.nonogram.solver.color.Color.BLANK;
import static com.verntrax.nonogram.solver.puzzle.RgbColoredBlock.fromSizeAndRgbColor;

public class HeartPuzzleSolverTestCaseProvider implements SolverTestCaseProvider {

    private static final RgbColor RED = RgbColor.fromRgbString("#ff0000");
    private static final RgbColor YELLOW = RgbColor.fromRgbString("#ffff00");

    @Override
    public Puzzle getPuzzle() {
        return Puzzle.builder()
                // rows 1-5
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(3, RED),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(4, RED),
                        fromSizeAndRgbColor(2, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, RED),
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(1, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(1, RED),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(1, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(6, YELLOW),
                        fromSizeAndRgbColor(2, RED)
                ))
                // rows 6-10
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(3, RED),
                        fromSizeAndRgbColor(4, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(3, RED),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(6, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(4, RED)
                ))
                .addRowBlocks(List.of(
                        fromSizeAndRgbColor(2, RED)
                ))
                // columns 1-5
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(5, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, RED),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                // columns 6-10
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(3, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(1, RED),
                        fromSizeAndRgbColor(2, YELLOW),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(2, RED),
                        fromSizeAndRgbColor(3, RED)
                ))
                .addColumnBlocks(List.of(
                        fromSizeAndRgbColor(5, RED)
                ))
                // build
                .build();
    }

    @Override
    public Solution getSolution() {
        return new Solution(new ArrayMatrix<>(10, 10,
                // 1-5
                BLANK, RED, RED, RED, BLANK,
                BLANK, RED, RED, RED, BLANK,

                RED, RED, BLANK, RED, RED,
                RED, RED, BLANK, RED, RED,

                RED, BLANK, BLANK, BLANK, RED,
                RED, BLANK, BLANK, BLANK, RED,

                RED, BLANK, YELLOW, YELLOW, BLANK,
                BLANK, YELLOW, YELLOW, BLANK, RED,

                RED, RED, YELLOW, YELLOW, YELLOW,
                YELLOW, YELLOW, YELLOW, RED, RED,

                // 6-10
                RED, RED, RED, YELLOW, YELLOW,
                YELLOW, YELLOW, RED, RED, RED,

                BLANK, RED, RED, RED, YELLOW,
                YELLOW, RED, RED, RED, BLANK,

                BLANK, BLANK, RED, RED, RED,
                RED, RED, RED, BLANK, BLANK,

                BLANK, BLANK, BLANK, RED, RED,
                RED, RED, BLANK, BLANK, BLANK,

                BLANK, BLANK, BLANK, BLANK, RED,
                RED, BLANK, BLANK, BLANK, BLANK
        ));
    }
}
