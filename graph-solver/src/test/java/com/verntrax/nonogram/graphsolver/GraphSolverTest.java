package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static com.verntrax.nonogram.solver.color.RgbColor.fromRgbString;
import static com.verntrax.nonogram.solver.puzzle.RgbColoredBlock.fromSizeAndRgbColor;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public abstract class GraphSolverTest {

    private static final RgbColor BLACK = fromRgbString("#000000");
    private static final RgbColor RED = fromRgbString("#ff0000");

    private GraphSolver solver;

    @BeforeEach
    public void createSolver() {
        solver = new GraphSolver(solverRunner());
    }

    @ParameterizedTest
    @MethodSource("shouldSolvePuzzle")
    public void shouldSolvePuzzle(SolverTestCaseProvider testCaseProvider) throws PuzzleUnsolvableException {
        // given
        Puzzle puzzle = testCaseProvider.getPuzzle();

        // when
        GraphSolver solver = new GraphSolver(solverRunner());
        Solution solution = solver.solve(puzzle);

        // then
        assertThat(solution)
                .as("solution")
                .isEqualTo(testCaseProvider.getSolution());
    }

    public static List<Arguments> shouldSolvePuzzle() {
        return List.of(
                Arguments.of(new HeartPuzzleSolverTestCaseProvider()),
                Arguments.of(new BirdPuzzleSolverTestCaseProvider()),
                Arguments.of(new BananaPuzzleSolverTestCaseProvider())
        );
    }

    @Test
    public void shouldThrowPuzzleUnsolvableExceptionIfPuzzleUnsolvable() {
        // given
        Puzzle puzzle = Puzzle.builder()
                .addRowBlocks(singletonList(fromSizeAndRgbColor(1, BLACK)))
                .addColumnBlocks(singletonList(fromSizeAndRgbColor(1, RED)))
                .build();

        // when
        ThrowableAssert.ThrowingCallable method = () -> solver.solve(puzzle);

        // then
        assertThatThrownBy(method)
                .as("exception thrown by solver")
                .isExactlyInstanceOf(PuzzleUnsolvableException.class);
    }

    @Test
    public void shouldThrowPuzzleUnsolvableExceptionIfPuzzleIncomplete() {
        // given
        Puzzle puzzle = Puzzle.builder()
                .addRowBlocks(singletonList(fromSizeAndRgbColor(1, BLACK)))
                .addColumnBlocks(emptyList())
                .build();

        // when
        ThrowableAssert.ThrowingCallable method = () -> solver.solve(puzzle);

        // then
        assertThatThrownBy(method)
                .as("exception thrown by solver")
                .isExactlyInstanceOf(PuzzleUnsolvableException.class);
    }

    protected abstract GraphSolverRunner solverRunner();
}
