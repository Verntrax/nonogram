open module com.verntrax.nonogram.graphsolver {

    exports com.verntrax.nonogram.graphsolver;

    requires com.verntrax.nonogram.commons;
    requires com.verntrax.nonogram.solver;
}
