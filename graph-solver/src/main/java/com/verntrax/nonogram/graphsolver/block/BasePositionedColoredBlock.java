package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.solver.color.ValidColor;

import java.util.Objects;

public final class BasePositionedColoredBlock implements PositionedColoredBlock {

    private final int size;
    private final ValidColor color;
    private final int position;

    protected BasePositionedColoredBlock(int position, int size, ValidColor color) {
        this.size = size;
        this.color = color;
        this.position = position;
    }

    @Override
    public int getStart() {
        return position;
    }

    @Override
    public ValidColor getColor() {
        return color;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof BasePositionedColoredBlock)) {
            return false;
        }
        BasePositionedColoredBlock block = (BasePositionedColoredBlock) other;
        return size == block.size
                && position == block.position
                && color.equals(block.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, size, color);
    }
}
