package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.solver.color.ValidColor;

import java.util.Objects;

public final class BaseColoredBlock implements ColoredBlock {

    private final int size;
    private final ValidColor color;

    protected BaseColoredBlock(int size, ValidColor color) {
        this.size = size;
        this.color = color;
    }

    @Override
    public int getSize() {
        return size;
    }

    public ValidColor getColor() {
        return color;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof BaseColoredBlock)) {
            return false;
        }
        BaseColoredBlock block = (BaseColoredBlock) other;
        return size == block.size
                && color.equals(block.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, color);
    }
}
