package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.solver.exception.SolverError;
import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solver.solution.Solution;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class GraphSolverRunner {

    private final ForkJoinPool pool;

    GraphSolverRunner() {
        this(new ForkJoinPool(1));
    }

    public GraphSolverRunner(ForkJoinPool pool) {
        this.pool = pool;
    }

    Solution execute(Callable<Solution> callable) throws PuzzleUnsolvableException {
        try {
            return pool.submit(callable).get();
        } catch (ExecutionException e) {
            throw handleAndTransformException(e);
        } catch (InterruptedException e) {
            throw handleAndTransformException(e);
        }
    }

    private PuzzleUnsolvableException handleAndTransformException(ExecutionException e) {
        Throwable cause = getOriginalCause(e);
        if (cause instanceof PuzzleUnsolvableException) {
            return (PuzzleUnsolvableException) cause;
        }
        throw new SolverError(e);
    }

    private RuntimeException handleAndTransformException(InterruptedException e) {
        Thread.currentThread().interrupt();
        return new RuntimeException(e);
    }

    private Throwable getOriginalCause(Throwable throwable) {
        Throwable cause = throwable.getCause();
        if (cause == null) {
            return throwable;
        }
        return getOriginalCause(cause);
    }
}
