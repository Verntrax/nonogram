package com.verntrax.nonogram.graphsolver.aggregator;

import com.verntrax.nonogram.graphsolver.block.PositionedBlock;

import java.util.*;

public class BlockAggregator {

    private final NavigableMap<Integer, PositionedBlock> blocksByStart = new TreeMap<>();

    public void addBlock(PositionedBlock block) {
        if (containsBlock(block)) {
            return;
        }
        PositionedBlock newBlock = buildNewBlock(block);
        removeObsoleteBlocks(newBlock);
        saveNewBlock(newBlock);
    }

    public boolean containsBlock(PositionedBlock block) {
        if (block.getSize() == 0) {
            return true;
        }
        return lastStartingBeforeStartOf(block)
                .map(b -> b.getEnd() >= block.getEnd())
                .orElse(false);
    }

    private PositionedBlock buildNewBlock(PositionedBlock block) {
        int newStart = findNewStart(block);
        int newEnd = findNewEnd(block);

        return PositionedBlock.fromStartAndEnd(newStart, newEnd);
    }

    private int findNewStart(PositionedBlock block) {
        return lastStartingBeforeStartOf(block)
                .filter(b -> b.getEnd() >= block.getStart())
                .map(b -> Math.min(b.getStart(), block.getStart()))
                .orElse(block.getStart());
    }

    private int findNewEnd(PositionedBlock block) {
        return lastStartingBeforeEndOf(block)
                .map(b -> Math.max(b.getEnd(), block.getEnd()))
                .orElse(block.getEnd());
    }

    private Optional<PositionedBlock> lastStartingBeforeStartOf(PositionedBlock block) {
        return Optional.ofNullable(blocksByStart.floorEntry(block.getStart()))
                .map(Map.Entry::getValue);
    }

    private Optional<PositionedBlock> lastStartingBeforeEndOf(PositionedBlock block) {
        return Optional.ofNullable(blocksByStart.floorEntry(block.getEnd()))
                .map(Map.Entry::getValue);
    }

    private void removeObsoleteBlocks(PositionedBlock newBlock) {
        blocksByStart.subMap(newBlock.getStart(), true, newBlock.getEnd(), true)
                .clear();
    }

    private void saveNewBlock(PositionedBlock block) {
        blocksByStart.put(block.getStart(), block);
    }

    public List<PositionedBlock> getBlocks() {
        return new ArrayList<>(blocksByStart.values());
    }
}
