package com.verntrax.nonogram.graphsolver.partialsolution;

import com.verntrax.nonogram.commons.matrix.ArrayMatrix;
import com.verntrax.nonogram.commons.matrix.Matrix;
import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.color.ValidColor;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;
import com.verntrax.nonogram.solver.solution.Solution;

import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class PartialSolution {

    private final Matrix<ColorField> availableColors;

    public PartialSolution(Puzzle puzzle) {
        availableColors = new ArrayMatrix<>(puzzle.getRowCount(), puzzle.getColumnCount());
        Set<ValidColor> allColorsInPuzzle = findAllColorsInPuzzle(puzzle);
        availableColors.populate((i, j) -> new ColorField(allColorsInPuzzle));
    }

    private Set<ValidColor> findAllColorsInPuzzle(Puzzle puzzle) {
        Set<ValidColor> result = IntStream.range(0, puzzle.getRowCount())
                .mapToObj(puzzle::getBlocksForRow)
                .flatMap(List::stream)
                .map(RgbColoredBlock::getColor)
                .collect(toSet());

        result.add(Color.BLANK);
        return result;
    }

    public boolean isRowChanged(int index) {
        return availableColors.row(index)
                .stream()
                .anyMatch(ColorField::isChanged);
    }

    public boolean isColumnChanged(int index) {
        return availableColors.column(index)
                .stream()
                .anyMatch(ColorField::isChanged);
    }

    public void resetChangeOnRow(int index) {
        availableColors.row(index)
                .forEach(ColorField::resetChanged);
    }

    public void resetChangeOnColumn(int index) {
        availableColors.column(index)
                .forEach(ColorField::resetChanged);
    }

    public boolean isSolved() {
        return availableColors.stream()
                .allMatch(ColorField::isUnequivocal);
    }

    public boolean isValid() {
        return availableColors.stream()
                .allMatch(ColorField::isValid);
    }

    public boolean isChanged() {
        return availableColors.stream()
                .anyMatch(ColorField::isChanged);
    }

    public List<Set<ValidColor>> getRow(int index) {
        return mapToColorList(availableColors.row(index));
    }

    public List<Set<ValidColor>> getColumn(int index) {
        return mapToColorList(availableColors.column(index));
    }

    private List<Set<ValidColor>> mapToColorList(List<ColorField> colorFields) {
        return colorFields.stream()
                .map(ColorField::getColors)
                .collect(toList());
    }

    public Solution toSolution() {
        return new Solution(availableColors.map(ColorField::asSingleColor));
    }

    public void updateRow(int index, List<Set<ValidColor>> update) {
        updateColorFields(availableColors.row(index), update);
    }

    public void updateColumn(int index, List<Set<ValidColor>> update) {
        updateColorFields(availableColors.column(index), update);
    }

    private void updateColorFields(List<ColorField> colorFields, List<Set<ValidColor>> update) {
        IntStream.range(0, update.size())
                .forEach(i -> colorFields.get(i)
                        .update(update.get(i)));
    }
}
