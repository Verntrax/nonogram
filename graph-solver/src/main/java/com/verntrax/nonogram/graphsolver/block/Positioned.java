package com.verntrax.nonogram.graphsolver.block;

public interface Positioned {

    int getStart();

    int getEnd();
}
