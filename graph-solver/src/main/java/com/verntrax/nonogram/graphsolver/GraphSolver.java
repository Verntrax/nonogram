package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.solver.Solver;
import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;

public class GraphSolver implements Solver {

    private final GraphSolverRunner runner;

    public GraphSolver(GraphSolverRunner runner) {
        this.runner = runner;
    }

    @Override
    public Solution solve(Puzzle puzzle) throws PuzzleUnsolvableException {
        validatePuzzle(puzzle);
        GraphSolverWorker worker = new GraphSolverWorker(puzzle);
        return runner.execute(worker::solve);
    }

    private void validatePuzzle(Puzzle puzzle) throws PuzzleUnsolvableException {
        if (puzzle.getRowCount() < 1 || puzzle.getColumnCount() < 1) {
            throw new PuzzleUnsolvableException();
        }
    }
}
