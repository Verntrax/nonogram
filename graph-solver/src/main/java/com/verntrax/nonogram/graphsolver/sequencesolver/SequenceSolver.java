package com.verntrax.nonogram.graphsolver.sequencesolver;

import com.verntrax.nonogram.commons.builder.BaseUsable;
import com.verntrax.nonogram.graphsolver.aggregator.ColoredBlockAggregator;
import com.verntrax.nonogram.graphsolver.block.ColoredBlock;
import com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock;
import com.verntrax.nonogram.graphsolver.flowgraph.LayeredFlowGraph;
import com.verntrax.nonogram.graphsolver.optionsfinder.OptionsFinder;
import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.color.ValidColor;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock.fromPositionSizeAndColor;
import static java.util.stream.Collectors.toList;

public class SequenceSolver extends BaseUsable {

    private final List<ColoredBlock> blocks;
    private final List<Set<ValidColor>> constraints;

    private OptionsFinder optionsFinder;

    private final List<Set<ValidColor>> result = new ArrayList<>();

    public SequenceSolver(List<RgbColoredBlock> blocks, List<? extends Set<ValidColor>> constraints) {
        this.blocks = toColoredBlocks(blocks);
        this.constraints = constraints.stream()
                .map(HashSet::new)
                .collect(toList());
    }

    private List<ColoredBlock> toColoredBlocks(List<RgbColoredBlock> rgbColoredBlocks) {
        return rgbColoredBlocks.stream()
                .map(block -> ColoredBlock.fromSizeAndColor(block.getSize(), block.getColor()))
                .collect(toList());
    }

    public List<Set<ValidColor>> solve() {
        checkAndUpdateStatus();
        initOptionsFinder();
        initResult();
        calculateResult();

        return result;
    }

    private void initOptionsFinder() {
        optionsFinder = new OptionsFinder(constraints);
    }

    private void initResult() {
        IntStream.range(0, constraints.size())
                .forEach(i -> result.add(new HashSet<>()));
    }

    private void calculateResult() {
        LayeredFlowGraph<PositionedColoredBlock, PositionedColoredBlock> graph = buildGraph();
        List<PositionedColoredBlock> resultAsBlocks = blocksFromGraph(graph);
        updateResult(resultAsBlocks);
    }

    private LayeredFlowGraph<PositionedColoredBlock, PositionedColoredBlock> buildGraph() {
        LayeredFlowGraph.Builder<PositionedColoredBlock, PositionedColoredBlock> builder = LayeredFlowGraph.builder();

        IntStream.range(1, blocks.size() + 1)
                .forEach(i -> populateGraphLayer(builder, i));

        return builder.addToLayer(0, fromPositionSizeAndColor(0, 0, Color.BLANK))
                .addToLayer(blocks.size() + 1, fromPositionSizeAndColor(constraints.size(), 0 , Color.BLANK))
                .withEdgePredicate(this::blocksCanCoexist)
                .withEdgeGenerator(this::blankBlockBetween)
                .build();
    }

    private void populateGraphLayer(LayeredFlowGraph.Builder<PositionedColoredBlock, PositionedColoredBlock> builder,
                                    int layer) {
        optionsFinder.optionsFor(blocks.get(layer - 1))
                .forEach(block -> builder.addToLayer(layer, block));
    }

    private boolean blocksCanCoexist(PositionedColoredBlock block1, PositionedColoredBlock block2) {
        return blocksNotOverlapping(block1, block2) && blocksCanHaveSpaceBetween(block1, block2);
    }

    private boolean blocksNotOverlapping(PositionedColoredBlock block1, PositionedColoredBlock block2) {
        int offset = block1.getColor().equals(block2.getColor()) ? 1 : 0;
        return block1.getEnd() + offset <= block2.getStart();
    }

    private boolean blocksCanHaveSpaceBetween(PositionedColoredBlock block1, PositionedColoredBlock block2) {
        return optionsFinder.hasOption(blankBlockBetween(block1, block2));
    }

    private PositionedColoredBlock blankBlockBetween(PositionedColoredBlock block1, PositionedColoredBlock block2) {
        return fromPositionSizeAndColor(block1.getEnd(), block2.getStart() - block1.getEnd(), Color.BLANK);
    }

    private List<PositionedColoredBlock> blocksFromGraph(
            LayeredFlowGraph<PositionedColoredBlock, PositionedColoredBlock> graph) {
        return Stream.of(graph.getNodeData(), graph.getEdgeData())
                .flatMap(List::stream)
                .collect(toList());
    }

    private void updateResult(List<PositionedColoredBlock> resultAsBlocks) {
        ColoredBlockAggregator aggregator = new ColoredBlockAggregator();
        resultAsBlocks.forEach(aggregator::addBlock);
        aggregator.getBlocks().forEach(this::addBlockToResult);
    }

    private void addBlockToResult(PositionedColoredBlock block) {
        IntStream.range(block.getStart(), block.getEnd())
                .forEach(i -> result.get(i).add(block.getColor()));
    }
}
