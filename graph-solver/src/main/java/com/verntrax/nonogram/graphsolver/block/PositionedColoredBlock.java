package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.solver.color.ValidColor;

public interface PositionedColoredBlock extends PositionedBlock, ColoredBlock {

    static PositionedColoredBlock fromPositionAndBlock(int position, ColoredBlock block) {
        return new BasePositionedColoredBlock(position, block.getSize(), block.getColor());
    }

    static PositionedColoredBlock fromColorAndBlock(ValidColor color, PositionedBlock block) {
        return new BasePositionedColoredBlock(block.getStart(), block.getSize(), color);
    }

    static PositionedColoredBlock fromPositionSizeAndColor(int position, int size, ValidColor color) {
        return new BasePositionedColoredBlock(position, size, color);
    }
}
