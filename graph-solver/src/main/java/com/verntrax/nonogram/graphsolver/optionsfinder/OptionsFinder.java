package com.verntrax.nonogram.graphsolver.optionsfinder;

import com.verntrax.nonogram.graphsolver.aggregator.ColoredBlockAggregator;
import com.verntrax.nonogram.graphsolver.block.Block;
import com.verntrax.nonogram.graphsolver.block.ColoredBlock;
import com.verntrax.nonogram.graphsolver.block.PositionedBlock;
import com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock;
import com.verntrax.nonogram.solver.color.ValidColor;

import java.util.*;
import java.util.stream.IntStream;

import static com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock.fromPositionAndBlock;
import static com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock.fromPositionSizeAndColor;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class OptionsFinder {

    private final ColoredBlockAggregator aggregator = new ColoredBlockAggregator();
    private final Map<ValidColor, List<PositionedColoredBlock>> orderedBlocks = new HashMap<>();

    public OptionsFinder(List<? extends Set<ValidColor>> availableColors) {
        initAggregator(availableColors);
    }

    private void initAggregator(List<? extends Set<ValidColor>> availableColors) {
        IntStream.range(0, availableColors.size())
                .mapToObj(i -> toUnitBlocksAtPosition(i, availableColors.get(i)))
                .flatMap(List::stream)
                .forEach(aggregator::addBlock);
    }

    private List<PositionedColoredBlock> toUnitBlocksAtPosition(int position, Set<ValidColor> colors) {
        return colors.stream()
                .map(color -> fromPositionSizeAndColor(position, 1, color))
                .collect(toList());
    }

    public Set<PositionedColoredBlock> optionsFor(ColoredBlock block) {
        List<PositionedColoredBlock> availableBlocks = orderedBlocks
                .computeIfAbsent(block.getColor(), this::orderedBlocksForColor);

        return availableBlocks.stream()
                .takeWhile(b -> b.getSize() >= block.getSize())
                .map(b -> allValidPositions(b, block))
                .flatMap(List::stream)
                .collect(toSet());
    }

    private List<PositionedColoredBlock> orderedBlocksForColor(ValidColor color) {
        List<PositionedColoredBlock> result = aggregator.getBlocksWithColor(color);
        result.sort(comparingInt(Block::getSize).reversed());
        return result;
    }

    private List<PositionedColoredBlock> allValidPositions(PositionedBlock enclosingBlock, ColoredBlock innerBlock) {
        return IntStream.range(enclosingBlock.getStart(), enclosingBlock.getEnd() - innerBlock.getSize() + 1)
                .mapToObj(i -> fromPositionAndBlock(i, innerBlock))
                .collect(toList());
    }

    public boolean hasOption(PositionedColoredBlock option) {
        return aggregator.containsBlock(option);
    }
}
