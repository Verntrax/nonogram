package com.verntrax.nonogram.graphsolver.block;

public interface Block {

    int getSize();
}
