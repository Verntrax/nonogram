package com.verntrax.nonogram.graphsolver.block;

import java.util.Objects;

public final class BasePositionedBlock implements PositionedBlock {

    private final int start;
    private final int end;

    protected BasePositionedBlock(int start, int end) {
        verifyStartEnd(start, end);
        this.start = start;
        this.end = end;
    }

    private void verifyStartEnd(int start, int end) {
        if (start > end || start < 0) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public int getStart() {
        return start;
    }

    @Override
    public int getEnd() {
        return end;
    }

    @Override
    public int getSize() {
        return end - start;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof BasePositionedBlock)) {
            return false;
        }
        BasePositionedBlock block = (BasePositionedBlock) other;
        return start == block.start
                && end == block.end;
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }
}
