package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.solver.color.ValidColor;

public interface ColoredBlock extends Block, Colored {

    static ColoredBlock fromSizeAndColor(int size, ValidColor color) {
        return new BaseColoredBlock(size, color);
    }
}
