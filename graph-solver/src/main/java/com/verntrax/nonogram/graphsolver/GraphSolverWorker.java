package com.verntrax.nonogram.graphsolver;

import com.verntrax.nonogram.commons.builder.BaseUsable;
import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.graphsolver.partialsolution.PartialSolution;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.graphsolver.sequencesolver.SequenceSolver;
import com.verntrax.nonogram.solver.solution.Solution;

import java.util.function.IntConsumer;
import java.util.stream.IntStream;

class GraphSolverWorker extends BaseUsable {

    private final Puzzle puzzle;
    private final PartialSolution solution;

    private boolean iterateRows = true;

    GraphSolverWorker(Puzzle puzzle) {
        this.puzzle = puzzle;
        solution = new PartialSolution(puzzle);
    }

    Solution solve() throws PuzzleUnsolvableException {
        checkAndUpdateStatus();
        doSolve();
        return solution.toSolution();
    }

    private void doSolve() throws PuzzleUnsolvableException {
        performFirstIteration();
        validateSolution();

        while (!solution.isSolved() && solution.isChanged()) {
            performIteration();
            validateSolution();
        }
    }

    private void performFirstIteration() {
        performIterationOnRowsUnconditionally();
        performIterationOnColumnsUnconditionally();
    }

    private void validateSolution() throws PuzzleUnsolvableException {
        if (!solution.isValid()) {
            throw new PuzzleUnsolvableException();
        }
    }

    private void performIteration() {
        if (iterateRows) {
            performIterationOnRows();
        } else {
            performIterationOnColumns();
        }
        iterateRows = !iterateRows;
    }

    private void performIterationOnRowsUnconditionally() {
        performActionOnRowRange(this::performIterationOnRowUnconditionally);
    }

    private void performIterationOnColumnsUnconditionally() {
        performActionOnColumnRange(this::performIterationOnColumnUnconditionally);
    }

    private void performIterationOnRows() {
        performActionOnRowRange(this::performIterationOnRow);
    }

    private void performIterationOnColumns() {
        performActionOnColumnRange(this::performIterationOnColumn);
    }

    private void performActionOnRowRange(IntConsumer action) {
        performActionOnRangeWithEnd(action, puzzle.getRowCount());
    }

    private void performActionOnColumnRange(IntConsumer action) {
        performActionOnRangeWithEnd(action, puzzle.getColumnCount());
    }

    private void performActionOnRangeWithEnd(IntConsumer action, int rangeEnd) {
        IntStream.range(0, rangeEnd)
                .parallel()
                .forEach(action);
    }

    private void performIterationOnRow(int index) {
        if (solution.isRowChanged(index)) {
            performIterationOnRowUnconditionally(index);
        }
    }

    private void performIterationOnColumn(int index) {
        if (solution.isColumnChanged(index)) {
            performIterationOnColumnUnconditionally(index);
        }
    }

    private void performIterationOnRowUnconditionally(int index) {
        solution.resetChangeOnRow(index);
        SequenceSolver partialSolver = sequenceSolverForRow(index);
        solution.updateRow(index, partialSolver.solve());
    }

    private void performIterationOnColumnUnconditionally(int index) {
        solution.resetChangeOnColumn(index);
        SequenceSolver partialSolver = sequenceSolverForColumn(index);
        solution.updateColumn(index, partialSolver.solve());
    }

    private SequenceSolver sequenceSolverForRow(int index) {
        return new SequenceSolver(puzzle.getBlocksForRow(index), solution.getRow(index));
    }

    private SequenceSolver sequenceSolverForColumn(int index) {
        return new SequenceSolver(puzzle.getBlocksForColumn(index), solution.getColumn(index));
    }
}
