package com.verntrax.nonogram.graphsolver.flowgraph;

class Node<T> {

    private final T data;

    private Boolean connected;

    Node(T data) {
        this.data = data;
    }

    T getData() {
        return data;
    }

    boolean isConnected() {
        return Boolean.TRUE.equals(connected);
    }

    boolean isConnectedNotSet() {
        return connected == null;
    }

    void setConnected(boolean connected) {
        this.connected = connected;
    }
}
