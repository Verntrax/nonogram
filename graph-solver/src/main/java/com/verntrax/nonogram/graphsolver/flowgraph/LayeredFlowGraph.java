package com.verntrax.nonogram.graphsolver.flowgraph;

import com.verntrax.nonogram.commons.builder.BaseBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LayeredFlowGraph<N, E> {

    private List<List<Node<N>>> layers = new ArrayList<>();
    private final List<E> edgeData = new ArrayList<>();

    private BiPredicate<? super N, ? super N> edgePredicate = (n1, n2) -> false;
    private BiFunction<? super N, ? super N, ? extends E> edgeGenerator = (n1, n2) -> null;

    public static <N, E> Builder<N, E> builder() {
        return new Builder<>();
    }

    public static class Builder<N, E> extends BaseBuilder<LayeredFlowGraph<N, E>> {

        private Builder() {
            super(new LayeredFlowGraph<>());
        }

        public Builder<N, E> addToLayer(int layerIndex, N data) {
            getCreation().addToLayer(layerIndex, data);
            return this;
        }

        public Builder<N, E> withEdgePredicate(BiPredicate<? super N, ? super N> edgePredicate) {
            getCreation().edgePredicate = edgePredicate;
            return this;
        }

        public Builder<N, E> withEdgeGenerator(BiFunction<? super N, ? super N, ? extends E> edgeGenerator) {
            getCreation().edgeGenerator = edgeGenerator;
            return this;
        }

        @Override
        protected void doBuild() {
            getCreation().buildEdges();
            getCreation().cleanNodes();
        }
    }

    private void addToLayer(int layerIndex, N data) {
        adjustLayersCount(layerIndex);
        layers.get(layerIndex).add(new Node<>(data));
    }

    private void adjustLayersCount(int layerIndex) {
        if (layerIndex >= layers.size()) {
            addNewLayers(layerIndex);
        }
    }

    private void addNewLayers(int newMaxIndex) {
        IntStream.range(layers.size(), newMaxIndex + 1)
                .mapToObj(i -> new ArrayList<Node<N>>())
                .forEach(layers::add);
    }

    private void buildEdges() {
        verifyLayersCount();
        layers.get(0)
                .forEach(edgeBuilderForFirstLayer());
    }

    private Consumer<Node<N>> edgeBuilderForFirstLayer() {
        return node -> edgeBuilderForLayer(0).test(node);
    }

    private boolean buildEdgesForNodeIfNecessary(int layerIndex, Node<N> node) {
        if (isLastLayer(layerIndex)) {
            node.setConnected(true);
            return true;
        }
        if (node.isConnectedNotSet()) {
            node.setConnected(buildEdgesForNode(layerIndex, node));
        }
        return node.isConnected();
    }

    private boolean isLastLayer(int layerIndex) {
        return layerIndex + 1 == layers.size();
    }

    private boolean buildEdgesForNode(int layerIndex, Node<N> node) {
        int nextLayerIndex = layerIndex + 1;

        return layers.get(nextLayerIndex)
                .stream()
                .filter(edgePredicateForNode(node))
                .filter(edgeBuilderForLayer(nextLayerIndex))
                .map(edgeGeneratorForNode(node))
                .peek(edgeData::add)
                .count() > 0;
    }

    private Function<Node<N>, E> edgeGeneratorForNode(Node<N> node) {
        return otherNode -> edgeGenerator.apply(node.getData(), otherNode.getData());
    }

    private Predicate<Node<N>> edgeBuilderForLayer(int layerIndex) {
        return node -> buildEdgesForNodeIfNecessary(layerIndex, node);
    }

    private Predicate<Node<N>> edgePredicateForNode(Node<N> node) {
        return otherNode -> edgePredicate.test(node.getData(), otherNode.getData());
    }

    private void verifyLayersCount() {
        if (layers.isEmpty()) {
            throw new IllegalStateException();
        }
    }

    private void cleanNodes() {
        layers = layers.stream()
                .map(this::cleanLayer)
                .collect(Collectors.toUnmodifiableList());
    }

    private List<Node<N>> cleanLayer(List<Node<N>> layer) {
        return layer.stream()
                .filter(Node::isConnected)
                .collect(Collectors.toUnmodifiableList());
    }

    public List<N> getNodeData() {
        return layers.stream()
                .flatMap(List::stream)
                .map(Node::getData)
                .collect(Collectors.toList());
    }

    public List<E> getEdgeData() {
        return Collections.unmodifiableList(edgeData);
    }
}
