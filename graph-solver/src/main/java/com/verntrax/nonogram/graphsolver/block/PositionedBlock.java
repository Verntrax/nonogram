package com.verntrax.nonogram.graphsolver.block;

public interface PositionedBlock extends Block {

    static PositionedBlock fromStartAndEnd(int start, int end) {
        return new BasePositionedBlock(start, end);
    }

    int getStart();

    default int getEnd() {
        return getStart() + getSize();
    }
}
