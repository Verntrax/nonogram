package com.verntrax.nonogram.graphsolver.aggregator;

import com.verntrax.nonogram.graphsolver.block.PositionedColoredBlock;
import com.verntrax.nonogram.solver.color.ValidColor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ColoredBlockAggregator {

    private final Map<ValidColor, BlockAggregator> aggregatorByColors = new HashMap<>();

    public void addBlock(PositionedColoredBlock block) {
        aggregatorByColors
                .computeIfAbsent(block.getColor(), c -> new BlockAggregator())
                .addBlock(block);
    }

    public boolean containsBlock(PositionedColoredBlock block) {
        if (block.getSize() == 0) {
            return true;
        }
        return Optional.ofNullable(aggregatorByColors.get(block.getColor()))
                .map(aggregator -> aggregator.containsBlock(block))
                .orElse(false);
    }

    public List<PositionedColoredBlock> getBlocksWithColor(ValidColor color) {
        return Optional.ofNullable(aggregatorByColors.get(color))
                .stream()
                .flatMap(aggregator -> coloredBlocksFromAggregator(color, aggregator))
                .collect(toList());
    }

    public List<PositionedColoredBlock> getBlocks() {
        return aggregatorByColors.entrySet().stream()
                .flatMap(entry -> coloredBlocksFromAggregator(entry.getKey(), entry.getValue()))
                .collect(toList());
    }

    private Stream<PositionedColoredBlock> coloredBlocksFromAggregator(ValidColor color, BlockAggregator aggregator) {
        return aggregator.getBlocks()
                .stream()
                .map(block -> PositionedColoredBlock.fromColorAndBlock(color, block));
    }
}
