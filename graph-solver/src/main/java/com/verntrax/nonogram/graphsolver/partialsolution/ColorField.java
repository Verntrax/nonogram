package com.verntrax.nonogram.graphsolver.partialsolution;

import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.color.ValidColor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toList;

class ColorField {

    private boolean changed;

    private final Set<ValidColor> colors;

    ColorField(Set<ValidColor> colors) {
        this.colors = new HashSet<>(colors);
    }

    Set<ValidColor> getColors() {
        return unmodifiableSet(colors);
    }

    boolean isValid() {
        return !colors.isEmpty();
    }

    boolean isUnequivocal() {
        return colors.size() == 1;
    }

    boolean isChanged() {
        return changed;
    }

    void resetChanged() {
        changed = false;
    }

    void update(Set<ValidColor> update) {
        List<ValidColor> toRemove = colors.stream()
                .filter(color -> !update.contains(color))
                .collect(toList());

        colors.removeAll(toRemove);
        changed |= !toRemove.isEmpty();
    }

    Color asSingleColor() {
        if (isUnequivocal()) {
            return colors.stream()
                    .findAny()
                    .orElseThrow(IllegalStateException::new);
        }
        if (isValid()) {
            return Color.UNKNOWN;
        }
        throw new IllegalStateException();
    }
}
