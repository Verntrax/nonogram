package com.verntrax.nonogram.graphsolver.block;

import com.verntrax.nonogram.solver.color.ValidColor;

public interface Colored {

    ValidColor getColor();
}
