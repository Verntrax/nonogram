package com.verntrax.nonogram.commons.builder;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class BuilderTest {

    private static class TestedBuilder extends BaseBuilder<List<Integer>> {

        private TestedBuilder() {
            super(new ArrayList<>());
        }

        @Override
        protected void doBuild() {
            getCreation().add(1);
        }
    }

    @Test
    public void shouldBuild() {
        // given
        TestedBuilder builder = new TestedBuilder();

        // when
        List<Integer> result = builder.build();

        // then
        assertThat(result)
                .as("builder creation")
                .isEqualTo(singletonList(1));
    }

    @Test
    public void shouldBuildTwice() throws Throwable {
        // given
        TestedBuilder builder = new TestedBuilder();
        ThrowingCallable action = builder::build;

        // when
        action.call();

        // then
        assertThatThrownBy(action)
                .as("exception thrown")
                .isExactlyInstanceOf(IllegalStateException.class);
    }
}
