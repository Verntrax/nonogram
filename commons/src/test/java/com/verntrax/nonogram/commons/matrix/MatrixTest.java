package com.verntrax.nonogram.commons.matrix;

import com.verntrax.nonogram.testutils.EqualityTest;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public abstract class MatrixTest implements EqualityTest<Matrix<String>> {

    public static final int TEST_MATRIX_ROWS = 2;
    public static final int TEST_MATRIX_COLS = 3;

    @Test
    public void shouldThrowWhenCreatingWithInvalidDataCount() {
        assertThatThrownBy(() -> new ArrayMatrix<>(TEST_MATRIX_ROWS, TEST_MATRIX_COLS, 1, 2, 3, 4, 5))
                .isExactlyInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldGetDimensions() {
        // given
        Matrix<String> matrix = testMatrix();

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(matrix.rowCount())
                    .as("matrix row count")
                    .isEqualTo(TEST_MATRIX_ROWS);
            softly.assertThat(matrix.columnCount())
                    .as("matrix column count")
                    .isEqualTo(TEST_MATRIX_COLS);
        });
    }

    @Test
    public void shouldCreateMatrixWithData() {
        // given
        Matrix<String> matrix = testMatrix();

        // then
        SoftAssertions softly = new SoftAssertions();
        IntStream.range(0, TEST_MATRIX_ROWS).forEach(i ->
                IntStream.range(0, TEST_MATRIX_COLS).forEach(j ->
                        softly.assertThat(matrix.get(i, j))
                                .as("value at (%s, %s)", i, j)
                                .isEqualTo(testMatrixValueAt(i, j))
                )
        );
        softly.assertAll();
    }

    @Test
    public void shouldCreateMatrixWithRowAndColumnCount() {
        // given
        int rowCount = 16;
        int colCount = 5;

        // when
        Matrix<Integer> matrix = new ArrayMatrix<>(rowCount, colCount);

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(matrix.rowCount())
                    .as("row count")
                    .isEqualTo(rowCount);
            softly.assertThat(matrix.columnCount())
                    .as("column count")
                    .isEqualTo(colCount);
        });
    }

    @Test
    public void shouldSetValueInMatrix() {
        // given
        String newValue = "20";
        int row = 0;
        int col = 2;

        Matrix<String> matrix = testMatrix();

        // when
        String previousValue = matrix.set(row, col, newValue);

        // then
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(previousValue)
                    .as("previous value")
                    .isEqualTo(testMatrixValueAt(row, col));
            softly.assertThat(matrix.get(row, col))
                    .as("new value")
                    .isEqualTo(newValue);
        });
    }

    @Test
    public void shouldGetRow() {
        // given
        Matrix<String> matrix = testMatrix();

        // when
        List<String> row = matrix.row(1);

        // then
        assertThat(row)
                .as("row 1")
                .hasSize(3)
                .containsExactly(testMatrixValueAt(1, 0), testMatrixValueAt(1, 1), testMatrixValueAt(1, 2));
    }

    @Test
    public void shouldGetColumn() {
        // given
        Matrix<String> matrix = testMatrix();

        // when
        List<String> row = matrix.column(2);

        // then
        assertThat(row)
                .as("column 2")
                .hasSize(2)
                .containsExactly(testMatrixValueAt(0, 2), testMatrixValueAt(1, 2));
    }

    @Test
    public void shouldSetValueOnGotRow() {
        // given
        String newValue = "7";
        int i = 0;
        int j = 2;

        Matrix<String> matrix = testMatrix();

        // when
        matrix.row(i).set(j, newValue);

        // then
        assertThat(matrix.get(i, j))
                .as("new value")
                .isEqualTo(newValue);
    }

    @Test
    public void shouldSetValueOnGotColumn() {
        // given
        String newValue = "9";
        int i = 1;
        int j = 0;

        Matrix<String> matrix = testMatrix();

        // when
        matrix.column(j).set(i, newValue);

        // then
        assertThat(matrix.get(i, j))
                .as("new value")
                .isEqualTo(newValue);
    }

    @Test
    public void shouldGetRows() {
        assertThat(testMatrix().rows())
                .containsExactly(
                        List.of(testMatrixValueAt(0, 0), testMatrixValueAt(0, 1), testMatrixValueAt(0, 2)),
                        List.of(testMatrixValueAt(1, 0), testMatrixValueAt(1, 1), testMatrixValueAt(1, 2))
                );
    }

    @Test
    public void shouldGetColumns() {
        assertThat(testMatrix().columns())
                .containsExactly(
                        List.of(testMatrixValueAt(0, 0), testMatrixValueAt(1, 0)),
                        List.of(testMatrixValueAt(0, 1), testMatrixValueAt(1, 1)),
                        List.of(testMatrixValueAt(0, 2), testMatrixValueAt(1, 2))
                );
    }

    @ParameterizedTest
    @ValueSource(ints = { -1, 2 })
    public void shouldThrowExceptionWhenGettingRowOutOfBound(int index) {
        // given
        Matrix<String> matrix = testMatrix();

        // when
        ThrowableAssert.ThrowingCallable method = () -> matrix.row(index);

        // then
        assertThatThrownBy(method).isExactlyInstanceOf(IndexOutOfBoundsException.class);
    }

    @ParameterizedTest
    @ValueSource(ints = { -1, 3 })
    public void shouldThrowExceptionWhenGettingColumnOutOfBound(int index) {
        // given
        Matrix<String> matrix = testMatrix();

        // when
        ThrowableAssert.ThrowingCallable method = () -> matrix.column(index);

        // then
        assertThatThrownBy(method).isExactlyInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    public void shouldThrowExceptionWhenGettingElementOutOfBound() {
        // given
        Matrix<String> matrix = testMatrix();

        // when
        ThrowableAssert.ThrowingCallable method = () -> matrix.get(2, 1);

        // then
        assertThatThrownBy(method).isExactlyInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    public void shouldThrowExceptionWhenSettingElementOutOfBound() {
        // given
        Matrix<String> matrix = testMatrix();

        // when
        ThrowableAssert.ThrowingCallable method = () -> matrix.set(0, 3, "5");

        // then
        assertThatThrownBy(method).isExactlyInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    public void shouldMakeStreamFromMatrix() {
        assertThat(testMatrix().stream())
                .containsExactly("0", "1", "2", "3", "4", "5");
    }

    @Test
    public void shouldMapMatrix() {
        // given
        Function<String, Integer> function = s -> Integer.parseInt(s) * 2;

        // when
        Matrix<Integer> newMatrix = testMatrix().map(function);

        // then
        SoftAssertions softly = new SoftAssertions();
        IntStream.range(0, TEST_MATRIX_ROWS).forEach(i ->
                IntStream.range(0, TEST_MATRIX_COLS).forEach(j ->
                        softly.assertThat(newMatrix.get(i, j))
                                .as("value at (%s, %s)", i, j)
                                .isEqualTo(function.apply(testMatrixValueAt(i, j)))
                )
        );
        softly.assertAll();
    }

    @Test
    public void shouldPopulateMatrix() {
        // given
        Matrix<String> matrix = emptyMatrixOfTestMatrixSize();

        // when
        matrix.populate(this::testMatrixValueAt);

        // then
        assertThat(matrix)
                .as("populated matrix")
                .isEqualTo(testMatrix());
    }

    @Test
    public void shouldCopyMatrix() {
        // given
        Matrix<String> newMatrix = testMatrix();

        // when
        Matrix<String> matrixCopy = newMatrix.copy();

        // then
        assertThat(matrixCopy)
                .as("matrix copy")
                .isNotSameAs(newMatrix)
                .isEqualTo(newMatrix);
    }

    public String[] testMatrixValues() {
        return new String[] {
                testMatrixValueAt(0, 0), testMatrixValueAt(0, 1), testMatrixValueAt(0, 2),
                testMatrixValueAt(1, 0), testMatrixValueAt(1, 1), testMatrixValueAt(1, 2),
        };
    }

    public String testMatrixValueAt(int i, int j) {
        return Integer.valueOf(3 * i + j).toString();
    }

    @Override
    public Matrix<String> newTestedObject() {
        return testMatrix();
    }

    protected abstract Matrix<String> testMatrix();

    protected abstract Matrix<String> emptyMatrixOfTestMatrixSize();
}
