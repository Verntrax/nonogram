package com.verntrax.nonogram.commons.matrix;

import java.util.List;

import static java.util.Collections.singletonList;

public class ArrayMatrixTest extends MatrixTest {

    @Override
    protected ArrayMatrix<String> testMatrix() {
        return new ArrayMatrix<>(TEST_MATRIX_ROWS, TEST_MATRIX_COLS, testMatrixValues());
    }

    @Override
    protected ArrayMatrix<String> emptyMatrixOfTestMatrixSize() {
        return new ArrayMatrix<>(TEST_MATRIX_ROWS, TEST_MATRIX_COLS);
    }

    @Override
    public List<Matrix<String>> differentObjects() {
        Matrix<String> differentMatrix = testMatrix();
        differentMatrix.set(1, 2, "xd");
        return singletonList(differentMatrix);
    }
}
