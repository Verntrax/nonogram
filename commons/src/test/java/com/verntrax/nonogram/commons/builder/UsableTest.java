package com.verntrax.nonogram.commons.builder;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

public class UsableTest {

    private static class TestedUsable extends BaseUsable {

        private void use() {
            checkAndUpdateStatus();
        }
    }

    @Test
    public void shouldUse() {
        // given
        TestedUsable usable = new TestedUsable();
        ThrowingCallable action = usable::use;

        // then
        assertThatCode(action)
                .as("exception thrown")
                .doesNotThrowAnyException();

        // and then
        assertThatThrownBy(action)
                .as("exception thrown")
                .isExactlyInstanceOf(IllegalStateException.class);
    }
}
