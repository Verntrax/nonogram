package com.verntrax.nonogram.commons.matrix;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.verntrax.nonogram.commons.matrix.Matrices.unmodifiableMatrix;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class UnmodifiableMatrixTest extends MatrixTest {

    @Test
    public void shouldReturnTheSameObjectWhenCreatingUnmodifiableMatrixFromUnmodifiableMatrix() {
        // given
        Matrix<String> matrix = testMatrix();

        // when
        Matrix<String> unmodifiableMatrix = unmodifiableMatrix(matrix);

        // then
        assertThat(matrix).isSameAs(unmodifiableMatrix);
    }

    @Test
    public void shouldThrowWhenSettingValue() {
        assertThatMethodUnsupported(() -> testMatrix().set(1, 2, "xd"));
    }

    @Test
    public void shouldThrowWhenSettingValueOnGotRow() {
        assertThatMethodUnsupported(() -> testMatrix().row(1).set(2, "xd"));
    }

    @Test
    public void shouldThrowWhenSettingValueOnGotColumn() {
        assertThatMethodUnsupported(() -> testMatrix().column(2).set(1, "xd"));
    }

    @Test
    public void shouldThrowWhenPopulatingMatrix() {
        assertThatMethodUnsupported(() -> emptyMatrixOfTestMatrixSize().populate(this::testMatrixValueAt));
    }

    private void assertThatMethodUnsupported(ThrowableAssert.ThrowingCallable method) {
        assertThatThrownBy(method).isExactlyInstanceOf(UnsupportedOperationException.class);
    }

    @Override
    public void shouldSetValueInMatrix() {
        // disabled
    }

    @Override
    public void shouldSetValueOnGotRow() {
        // disabled
    }

    @Override
    public void shouldSetValueOnGotColumn() {
        // disabled
    }

    @Override
    public void shouldThrowExceptionWhenSettingElementOutOfBound() {
        // disabled
    }

    @Override
    public void shouldPopulateMatrix() {
        // disabled
    }

    @Override
    protected Matrix<String> testMatrix() {
        return unmodifiableMatrix(delegateTestMatrix());
    }

    @Override
    protected Matrix<String> emptyMatrixOfTestMatrixSize() {
        return unmodifiableMatrix(new ArrayMatrix<>(TEST_MATRIX_ROWS, TEST_MATRIX_COLS));
    }

    private Matrix<String> delegateTestMatrix() {
        return new ArrayMatrix<>(TEST_MATRIX_ROWS, TEST_MATRIX_COLS, testMatrixValues());
    }

    @Override
    public List<Matrix<String>> differentObjects() {
        Matrix<String> differentMatrix = delegateTestMatrix();
        differentMatrix.set(0, 1, "xd");
        return singletonList(unmodifiableMatrix(differentMatrix));
    }
}
