package com.verntrax.nonogram.commons.matrix;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Matrix<T> {

    void populate(BiFunction<Integer, Integer, ? extends T> function);

    T get(int row, int col);

    T set(int row, int col, T value);

    List<T> row(int row);

    List<T> column(int column);

    Stream<T> stream();

    <N> Matrix<N> map(Function<? super T, N> function);

    List<List<T>> rows();

    List<List<T>> columns();

    int rowCount();

    int columnCount();

    default Matrix<T> copy() {
        return map(t -> t);
    }
}
