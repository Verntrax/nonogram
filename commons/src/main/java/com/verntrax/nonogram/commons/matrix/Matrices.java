package com.verntrax.nonogram.commons.matrix;

public interface Matrices {

    static <T> Matrix<T> unmodifiableMatrix(Matrix<T> matrix) {
        if (matrix instanceof UnmodifiableMatrix) {
            return matrix;
        }
        return new UnmodifiableMatrix<>(matrix);
    }
}
