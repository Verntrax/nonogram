package com.verntrax.nonogram.commons.builder;

public abstract class BaseUsable {

    private boolean used;

    protected void checkAndUpdateStatus() {
        if (used) {
            throw new IllegalStateException();
        }
        used = true;
    }
}
