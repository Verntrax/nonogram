package com.verntrax.nonogram.commons.matrix;

import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

final class UnmodifiableMatrix<T> implements Matrix<T> {

    private final Matrix<T> matrix;

    UnmodifiableMatrix(Matrix<T> matrix) {
        this.matrix = matrix;
    }

    @Override
    public void populate(BiFunction<Integer, Integer, ? extends T> function) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T get(int row, int col) {
        return matrix.get(row, col);
    }

    @Override
    public T set(int row, int col, T value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> row(int row) {
        return unmodifiableList(matrix.row(row));
    }

    @Override
    public List<T> column(int column) {
        return unmodifiableList(matrix.column(column));
    }

    @Override
    public Stream<T> stream() {
        return matrix.stream();
    }

    @Override
    public <N> Matrix<N> map(Function<? super T, N> function) {
        return matrix.map(function);
    }

    @Override
    public List<List<T>> rows() {
        return makeListsUnmodifiable(matrix.rows());
    }

    @Override
    public List<List<T>> columns() {
        return makeListsUnmodifiable(matrix.columns());
    }

    private List<List<T>> makeListsUnmodifiable(List<List<T>> lists) {
        return lists.stream()
                .map(Collections::unmodifiableList)
                .collect(toList());
    }

    @Override
    public int rowCount() {
        return matrix.rowCount();
    }

    @Override
    public int columnCount() {
        return matrix.columnCount();
    }

    @Override
    public int hashCode() {
        return matrix.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return matrix.equals(other);
    }
}
