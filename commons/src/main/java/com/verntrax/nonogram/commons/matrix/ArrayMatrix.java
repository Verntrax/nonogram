package com.verntrax.nonogram.commons.matrix;

import java.util.AbstractList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ArrayMatrix<T> implements Matrix<T> {

    private final int rowCount;
    private final int columnCount;

    private final Object[][] data;

    public ArrayMatrix(int rowCount, int columnCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;

        data = new Object[rowCount][columnCount];
    }

    @SafeVarargs
    public ArrayMatrix(int rowCount, int columnCount, T... data) {
        if (data.length != rowCount * columnCount) {
            throw new IllegalArgumentException();
        }
        this.rowCount = rowCount;
        this.columnCount = columnCount;

        this.data = new Object[rowCount][columnCount];
        copyData(data);
    }

    private void copyData(T[] dataAsArray) {
        populate((i, j) -> dataAsArray[i * columnCount + j]);
    }

    @Override
    public void populate(BiFunction<Integer, Integer, ? extends T> function) {
        IntStream.range(0, rowCount).forEach(i ->
                IntStream.range(0, columnCount).forEach(j ->
                        data[i][j] = function.apply(i, j)
                )
        );
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int row, int col) {
        if (row < 0 || row >= rowCount || col < 0 || col >= columnCount) {
            throw new IndexOutOfBoundsException();
        }
        return (T) data[row][col];
    }

    @Override
    public T set(int row, int col, T value) {
        T previousValue = get(row, col);
        data[row][col] = value;
        return previousValue;
    }

    @Override
    public List<T> row(int row) {
        if (row < 0 || row >= rowCount) {
            throw new IndexOutOfBoundsException();
        }
        return new Row(row);
    }

    @Override
    public List<T> column(int column) {
        if (column < 0 || column >= columnCount) {
            throw new IndexOutOfBoundsException();
        }
        return new Column(column);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Stream<T> stream() {
        return Stream.of(data)
                .flatMap(Stream::of)
                .map(o -> (T) o);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <N> ArrayMatrix<N> map(Function<? super T, N> function) {
        ArrayMatrix<N> newMatrix = new ArrayMatrix<>(rowCount, columnCount);
        newMatrix.populate((i, j) -> function.apply((T) data[i][j]));
        return newMatrix;
    }

    @Override
    public List<List<T>> rows() {
        return IntStream.range(0, rowCount)
                .mapToObj(Row::new)
                .collect(toList());
    }

    @Override
    public List<List<T>> columns() {
        return IntStream.range(0, columnCount)
                .mapToObj(Column::new)
                .collect(toList());
    }

    public int rowCount() {
        return rowCount;
    }

    public int columnCount() {
        return columnCount;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Matrix)) {
            return false;
        }
        Matrix<?> m = (Matrix<?>) other;
        return rows().equals(m.rows());
    }

    @Override
    public int hashCode() {
        return rows().hashCode();
    }

    private class Row extends AbstractList<T> {

        private final int row;

        private Row(int row) {
            this.row = row;
        }

        @Override
        public T get(int index) {
            return ArrayMatrix.this.get(row, index);
        }

        @Override
        public T set(int index, T value) {
            return ArrayMatrix.this.set(row, index, value);
        }

        @Override
        public int size() {
            return columnCount;
        }
    }

    private class Column extends AbstractList<T> {

        private final int col;

        private Column(int col) {
            this.col = col;
        }

        @Override
        public T get(int index) {
            return ArrayMatrix.this.get(index, col);
        }

        @Override
        public T set(int index, T value) {
            return ArrayMatrix.this.set(index, col, value);
        }

        @Override
        public int size() {
            return rowCount;
        }
    }
}
