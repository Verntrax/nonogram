package com.verntrax.nonogram.commons.builder;

public abstract class BaseBuilder<T> extends BaseUsable {

    private final T creation;

    public BaseBuilder(T creation) {
        this.creation = creation;
    }

    protected T getCreation() {
        return creation;
    }

    public T build() {
        checkAndUpdateStatus();
        doBuild();
        return creation;
    }

    protected abstract void doBuild();
}
