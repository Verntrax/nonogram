open module com.verntrax.nonogram.solver.service {

    exports com.verntrax.nonogram.solverservice;
    exports com.verntrax.nonogram.solverservice.converter;
    exports com.verntrax.nonogram.solverservice.converter.exception;
    exports com.verntrax.nonogram.solverservice.dto;

    requires com.verntrax.nonogram.commons;
    requires com.verntrax.nonogram.solver;

    requires java.validation;
}
