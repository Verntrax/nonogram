package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.color.Color;

public class ColorToStringConverter implements Converter<Color, String> {

    @Override
    public String convert(Color color) {
        return color.toString();
    }
}
