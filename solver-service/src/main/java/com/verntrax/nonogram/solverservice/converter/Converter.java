package com.verntrax.nonogram.solverservice.converter;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

public interface Converter<S, T> {

    T convert(S source);

    default List<T> convertList(Collection<? extends S> source) {
        return source.stream()
                .map(this::convert)
                .collect(toList());
    }
}
