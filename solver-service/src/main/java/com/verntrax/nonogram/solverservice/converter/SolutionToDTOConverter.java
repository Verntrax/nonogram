package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.commons.matrix.Matrix;
import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.solution.Solution;
import com.verntrax.nonogram.solverservice.dto.SolutionDTO;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class SolutionToDTOConverter implements Converter<Solution, SolutionDTO> {

    private final Converter<Color, String> colorConverter;

    public SolutionToDTOConverter(Converter<Color, String> colorConverter) {
        this.colorConverter = colorConverter;
    }

    @Override
    public SolutionDTO convert(Solution solution) {
        SolutionDTO dto = new SolutionDTO();

        List<List<String>> convertedSolution = convertSolutionMatrix(solution.asMatrix());
        dto.setSolution(convertedSolution);

        return dto;
    }

    private List<List<String>> convertSolutionMatrix(Matrix<Color> solution) {
        return solution.rows()
                .stream()
                .map(colorConverter::convertList)
                .collect(toList());
    }
}
