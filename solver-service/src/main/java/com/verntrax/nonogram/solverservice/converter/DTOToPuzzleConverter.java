package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.exception.InvalidPuzzleException;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;
import com.verntrax.nonogram.solverservice.converter.exception.ConversionException;
import com.verntrax.nonogram.solverservice.dto.PuzzleDTO;
import com.verntrax.nonogram.solverservice.dto.RgbColoredBlockDTO;

import java.util.List;
import java.util.function.Consumer;

public class DTOToPuzzleConverter implements Converter<PuzzleDTO, Puzzle> {

    private final Converter<RgbColoredBlockDTO, RgbColoredBlock> blockConverter;

    public DTOToPuzzleConverter(Converter<RgbColoredBlockDTO, RgbColoredBlock> blockConverter) {
        this.blockConverter = blockConverter;
    }

    @Override
    public Puzzle convert(PuzzleDTO dto) {
        Puzzle.Builder builder = Puzzle.builder();

        addRowsToBuilder(builder, dto);
        addColumnsToBuilder(builder, dto);

        return build(builder);
    }

    private void addRowsToBuilder(Puzzle.Builder builder, PuzzleDTO dto) {
        forEachConverted(dto.getRowBlocks(), builder::addRowBlocks);
    }

    private void addColumnsToBuilder(Puzzle.Builder builder, PuzzleDTO dto) {
        forEachConverted(dto.getColumnBlocks(), builder::addColumnBlocks);
    }

    private Puzzle build(Puzzle.Builder builder) {
        try {
            return builder.build();
        } catch (InvalidPuzzleException e) {
            throw new ConversionException(e);
        }
    }

    private <B extends RgbColoredBlockDTO> void forEachConverted(List<List<B>> blocks,
                                                                 Consumer<List<RgbColoredBlock>> action) {
        blocks.stream()
                .map(blockConverter::convertList)
                .forEachOrdered(action);
    }
}
