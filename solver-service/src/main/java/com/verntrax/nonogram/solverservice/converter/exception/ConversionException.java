package com.verntrax.nonogram.solverservice.converter.exception;

public class ConversionException extends RuntimeException {

    public ConversionException(Throwable t) {
        super(t);
    }
}
