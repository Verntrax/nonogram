package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solverservice.converter.exception.ConversionException;

public class StringToRgbColorConverter implements Converter<String, RgbColor> {

    @Override
    public RgbColor convert(String rgbString) {
        try {
            return RgbColor.fromRgbString(rgbString);
        } catch (NumberFormatException e) {
            throw new ConversionException(e);
        }
    }
}
