package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;
import com.verntrax.nonogram.solverservice.dto.RgbColoredBlockDTO;

public class DTOToRgbColoredBlockConverter implements Converter<RgbColoredBlockDTO, RgbColoredBlock> {

    private final Converter<String, RgbColor> colorConverter;

    public DTOToRgbColoredBlockConverter(Converter<String, RgbColor> colorConverter) {
        this.colorConverter = colorConverter;
    }

    @Override
    public RgbColoredBlock convert(RgbColoredBlockDTO dto) {
        RgbColor color = colorConverter.convert(dto.getColor());
        return RgbColoredBlock.fromSizeAndRgbColor(dto.getSize(), color);
    }
}
