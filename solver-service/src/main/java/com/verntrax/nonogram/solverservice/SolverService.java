package com.verntrax.nonogram.solverservice;

import com.verntrax.nonogram.solver.Solver;
import com.verntrax.nonogram.solver.exception.PuzzleUnsolvableException;
import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.solution.Solution;
import com.verntrax.nonogram.solverservice.dto.PuzzleDTO;
import com.verntrax.nonogram.solverservice.converter.Converter;
import com.verntrax.nonogram.solverservice.dto.SolutionDTO;

public class SolverService {

    private final Solver solver;

    private final Converter<PuzzleDTO, Puzzle> inputConverter;
    private final Converter<Solution, SolutionDTO> outputConverter;

    public SolverService(Solver solver,
                         Converter<PuzzleDTO, Puzzle> inputConverter,
                         Converter<Solution, SolutionDTO> outputConverter) {
        this.solver = solver;
        this.inputConverter = inputConverter;
        this.outputConverter = outputConverter;
    }

    public SolutionDTO solve(PuzzleDTO puzzleDTO) throws PuzzleUnsolvableException {
        Puzzle puzzle = inputConverter.convert(puzzleDTO);
        Solution solution = solver.solve(puzzle);
        return outputConverter.convert(solution);
    }
}
