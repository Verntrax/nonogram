package com.verntrax.nonogram.solverservice.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class PuzzleDTO implements DTO {

    @NotNull
    @Size(min = 1)
    private List<@NotNull List<@NotNull @Valid RgbColoredBlockDTO>> rowBlocks;

    @NotNull
    @Size(min = 1)
    private List<@NotNull List<@NotNull @Valid RgbColoredBlockDTO>> columnBlocks;

    public List<List<RgbColoredBlockDTO>> getRowBlocks() {
        return rowBlocks;
    }

    public void setRowBlocks(List<List<RgbColoredBlockDTO>> rowBlocks) {
        this.rowBlocks = rowBlocks;
    }

    public List<List<RgbColoredBlockDTO>> getColumnBlocks() {
        return columnBlocks;
    }

    public void setColumnBlocks(List<List<RgbColoredBlockDTO>> columnBlocks) {
        this.columnBlocks = columnBlocks;
    }
}
