package com.verntrax.nonogram.solverservice.dto;

import com.verntrax.nonogram.solver.color.Color;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public class SolutionDTO implements DTO {

    @NotNull
    @Size(min = 1)
    private List<@NotNull @Size(min = 1) List<@NotNull @Pattern(regexp = Color.COLOR_REGEX) String>> solution;

    public List<List<String>> getSolution() {
        return solution;
    }

    public void setSolution(List<List<String>> solution) {
        this.solution = solution;
    }
}
