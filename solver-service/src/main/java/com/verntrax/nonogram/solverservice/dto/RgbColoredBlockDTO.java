package com.verntrax.nonogram.solverservice.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

public class RgbColoredBlockDTO implements DTO {

    @NotNull
    @Pattern(regexp = "#[0-9a-f]{6}")
    private String color;

    @NotNull
    @Positive
    private Integer size;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
