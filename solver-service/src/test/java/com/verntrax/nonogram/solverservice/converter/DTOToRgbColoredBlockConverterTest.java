package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;
import com.verntrax.nonogram.solverservice.dto.RgbColoredBlockDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DTOToRgbColoredBlockConverterTest {

    private static final String COLOR_STRING1 = "1";
    private static final String COLOR_STRING2 = "2";
    private static final String COLOR_STRING3 = "3";

    private static final RgbColor COLOR1 = mock(RgbColor.class);
    private static final RgbColor COLOR2 = mock(RgbColor.class);
    private static final RgbColor COLOR3 = mock(RgbColor.class);

    private Converter<RgbColoredBlockDTO, RgbColoredBlock> converter;

    @BeforeEach
    @SuppressWarnings("unchecked")
    public void initMocks() {
        Converter<String, RgbColor> colorConverter = mock(Converter.class);
        converter = new DTOToRgbColoredBlockConverter(colorConverter);

        when(colorConverter.convert(COLOR_STRING1)).thenReturn(COLOR1);
        when(colorConverter.convert(COLOR_STRING2)).thenReturn(COLOR2);
        when(colorConverter.convert(COLOR_STRING3)).thenReturn(COLOR3);
    }

    @ParameterizedTest
    @MethodSource("shouldConvertDTOToBlock")
    public void shouldConvertDTOToBlock(RgbColoredBlockDTO dto, RgbColoredBlock expected) {
        // when
        RgbColoredBlock result = converter.convert(dto);

        // then
        assertThat(result).isEqualTo(expected);
    }

    public static List<Arguments> shouldConvertDTOToBlock() {
        return List.of(
                Arguments.of(newColoredBlockDTO(4, COLOR_STRING1), newColoredBlock(4, COLOR1)),
                Arguments.of(newColoredBlockDTO(1, COLOR_STRING2), newColoredBlock(1, COLOR2)),
                Arguments.of(newColoredBlockDTO(2, COLOR_STRING3), newColoredBlock(2, COLOR3)));
    }

    private static RgbColoredBlockDTO newColoredBlockDTO(int size, String colorString) {
        RgbColoredBlockDTO dto = new RgbColoredBlockDTO();
        dto.setSize(size);
        dto.setColor(colorString);
        return dto;
    }

    private static RgbColoredBlock newColoredBlock(int size, RgbColor color) {
        return RgbColoredBlock.fromSizeAndRgbColor(size, color);
    }
}
