package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.commons.matrix.ArrayMatrix;
import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.solution.Solution;
import com.verntrax.nonogram.solverservice.dto.SolutionDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SolutionToDTOConverterTest {

    private static final Color COLOR1 = mock(Color.class);
    private static final Color COLOR2 = mock(Color.class);
    private static final Color COLOR3 = mock(Color.class);

    private static final String COLOR_STRING1 = "1";
    private static final String COLOR_STRING2 = "2";
    private static final String COLOR_STRING3 = "3";

    private Converter<Solution, SolutionDTO> converter;

    @BeforeEach
    @SuppressWarnings("unchecked")
    public void initMocks() {
        Converter<Color, String> colorConverter = mock(Converter.class);

        converter = new SolutionToDTOConverter(colorConverter);

        when(colorConverter.convertList(anyList())).thenCallRealMethod();
        when(colorConverter.convert(COLOR1)).thenReturn(COLOR_STRING1);
        when(colorConverter.convert(COLOR2)).thenReturn(COLOR_STRING2);
        when(colorConverter.convert(COLOR3)).thenReturn(COLOR_STRING3);
    }

    @ParameterizedTest
    @MethodSource("shouldConvertSolutionToDTO")
    public void shouldConvertSolutionToDTO(Solution solution, List<List<String>> expected) {
        // when
        SolutionDTO dto = converter.convert(solution);

        // then
        assertThat(dto.getSolution()).isEqualTo(expected);
    }

    public static List<Arguments> shouldConvertSolutionToDTO() {
        return List.of(
                Arguments.of(
                        newSolution(1, 1, COLOR1),
                        List.of(List.of(COLOR_STRING1))),
                Arguments.of(
                        newSolution(2, 3,
                                COLOR1, COLOR2, COLOR3,
                                COLOR2, COLOR2, COLOR1),
                        List.of(
                                List.of(COLOR_STRING1, COLOR_STRING2, COLOR_STRING3),
                                List.of(COLOR_STRING2, COLOR_STRING2, COLOR_STRING1))),
                Arguments.of(
                        newSolution(3, 4,
                                COLOR2, COLOR1, COLOR2, COLOR3,
                                COLOR3, COLOR1, COLOR1, COLOR2,
                                COLOR1, COLOR2, COLOR1, COLOR1),
                        List.of(
                                List.of(COLOR_STRING2, COLOR_STRING1, COLOR_STRING2, COLOR_STRING3),
                                List.of(COLOR_STRING3, COLOR_STRING1, COLOR_STRING1, COLOR_STRING2),
                                List.of(COLOR_STRING1, COLOR_STRING2, COLOR_STRING1, COLOR_STRING1))));
    }

    private static Solution newSolution(int rows, int columns, Color... values) {
        return new Solution(new ArrayMatrix<>(rows, columns, values));
    }
}
