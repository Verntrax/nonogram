package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.color.Color;
import com.verntrax.nonogram.solver.color.RgbColor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.verntrax.nonogram.solver.color.Color.BLANK;
import static com.verntrax.nonogram.solver.color.Color.UNKNOWN;
import static org.assertj.core.api.Assertions.assertThat;

public class ColorToStringConverterTest {

    private final Converter<Color, String> converter = new ColorToStringConverter();

    @ParameterizedTest
    @MethodSource("shouldConvertColorToString")
    public void shouldConvertColorToString(Color color, String expected) {
        // when
        String actual = converter.convert(color);

        // then
        assertThat(actual).isEqualTo(expected);
    }

    public static Stream<Arguments> shouldConvertColorToString() {
        return Stream.of(
                Arguments.of(RgbColor.fromRgbString("#4567ff"), "#4567ff"),
                Arguments.of(UNKNOWN, "UNKNOWN"),
                Arguments.of(BLANK, "BLANK")
        );
    }
}
