package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.puzzle.Puzzle;
import com.verntrax.nonogram.solver.puzzle.RgbColoredBlock;
import com.verntrax.nonogram.solverservice.converter.exception.ConversionException;
import com.verntrax.nonogram.solverservice.dto.PuzzleDTO;
import com.verntrax.nonogram.solverservice.dto.RgbColoredBlockDTO;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DTOToPuzzleConverterTest {

    private static final RgbColoredBlock BLOCK1 = mock(RgbColoredBlock.class);
    private static final RgbColoredBlock BLOCK2 = mock(RgbColoredBlock.class);
    private static final RgbColoredBlock BLOCK3 = mock(RgbColoredBlock.class);

    private static final RgbColoredBlockDTO BLOCK_DTO1 = mock(RgbColoredBlockDTO.class);
    private static final RgbColoredBlockDTO BLOCK_DTO2 = mock(RgbColoredBlockDTO.class);
    private static final RgbColoredBlockDTO BLOCK_DTO3 = mock(RgbColoredBlockDTO.class);

    private Converter<PuzzleDTO, Puzzle> converter;

    @BeforeEach
    @SuppressWarnings("unchecked")
    public void initMocks() {
        Converter<RgbColoredBlockDTO, RgbColoredBlock> blockConverter = mock(Converter.class);

        converter = new DTOToPuzzleConverter(blockConverter);

        when(blockConverter.convertList(anyList())).thenCallRealMethod();
        when(blockConverter.convert(BLOCK_DTO1)).thenReturn(BLOCK1);
        when(blockConverter.convert(BLOCK_DTO2)).thenReturn(BLOCK2);
        when(blockConverter.convert(BLOCK_DTO3)).thenReturn(BLOCK3);
    }

    @Test
    public void shouldConvertDTOToPuzzle() {
        // given
        PuzzleDTO dto = new PuzzleDTO();
        dto.setRowBlocks(List.of(
                List.of(BLOCK_DTO1),
                List.of(BLOCK_DTO2, BLOCK_DTO1),
                List.of(BLOCK_DTO2, BLOCK_DTO3)
        ));
        dto.setColumnBlocks(List.of(
                List.of(BLOCK_DTO3, BLOCK_DTO1, BLOCK_DTO2),
                List.of(BLOCK_DTO3, BLOCK_DTO3)
        ));

        // when
        Puzzle puzzle = converter.convert(dto);

        // then
        assertRows(puzzle, List.of(
                List.of(BLOCK1),
                List.of(BLOCK2, BLOCK1),
                List.of(BLOCK2, BLOCK3)
        ));
        assertColumns(puzzle, List.of(
                List.of(BLOCK3, BLOCK1, BLOCK2),
                List.of(BLOCK3, BLOCK3)
        ));
    }

    @Test
    public void shouldThrowExceptionWhenRowsAreEmpty() {
        // given
        PuzzleDTO dto = new PuzzleDTO();
        dto.setRowBlocks(List.of());
        dto.setColumnBlocks(List.of(List.of(BLOCK_DTO1)));

        // when
        ThrowableAssert.ThrowingCallable method = () -> converter.convert(dto);

        // then
        assertThatThrownBy(method).isInstanceOf(ConversionException.class);
    }

    @Test
    public void shouldThrowExceptionWhenColumnsAreEmpty() {
        // given
        PuzzleDTO dto = new PuzzleDTO();
        dto.setRowBlocks(List.of(List.of(BLOCK_DTO1)));
        dto.setColumnBlocks(List.of());

        // when
        ThrowableAssert.ThrowingCallable method = () -> converter.convert(dto);

        // then
        assertThatThrownBy(method).isInstanceOf(ConversionException.class);
    }

    private void assertRows(Puzzle puzzle, List<List<RgbColoredBlock>> rowBlocks) {
        assertThat(puzzle.getRowCount())
                .as("row count")
                .isEqualTo(rowBlocks.size());

        SoftAssertions softly = new SoftAssertions();
        IntStream.range(0, rowBlocks.size()).forEach(i ->
                softly.assertThat(puzzle.getBlocksForRow(i))
                        .as("blocks from row %s", i)
                        .containsExactlyElementsOf(rowBlocks.get(i))
        );
        softly.assertAll();
    }

    private void assertColumns(Puzzle puzzle, List<List<RgbColoredBlock>> columnBlocks) {
        assertThat(puzzle.getColumnCount())
                .as("column count")
                .isEqualTo(columnBlocks.size());

        SoftAssertions softly = new SoftAssertions();
        IntStream.range(0, columnBlocks.size()).forEach(i ->
                softly.assertThat(puzzle.getBlocksForColumn(i))
                        .as("blocks from column %s", i)
                        .containsExactlyElementsOf(columnBlocks.get(i))
        );
        softly.assertAll();
    }
}
