package com.verntrax.nonogram.solverservice.converter;

import com.verntrax.nonogram.solver.color.RgbColor;
import com.verntrax.nonogram.solverservice.converter.exception.ConversionException;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class StringToRgbColorConverterTest {

    private final Converter<String, RgbColor> converter = new StringToRgbColorConverter();

    @ParameterizedTest
    @ValueSource(strings = { "#ff0044", "#551146" })
    public void shouldConvertValidString(String colorString) {
        // when
        RgbColor result = converter.convert(colorString);

        // then
        assertThat(result).isEqualTo(RgbColor.fromRgbString(colorString));
    }

    @ParameterizedTest
    @ValueSource(strings = { "555555", "#0099gf", "#77880" })
    public void shouldThrowExceptionForInvalidString(String invalidString) {
        // when
        ThrowableAssert.ThrowingCallable method = () -> converter.convert(invalidString);

        // then
        assertThatThrownBy(method).isInstanceOf(ConversionException.class);
    }
}
