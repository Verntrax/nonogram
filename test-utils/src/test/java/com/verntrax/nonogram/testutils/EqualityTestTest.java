package com.verntrax.nonogram.testutils;

import java.util.List;

public class EqualityTestTest implements EqualityTest<Integer> {

    @Override
    public Integer newTestedObject() {
        return 5;
    }

    @Override
    public List<Integer> differentObjects() {
        return List.of(-5, 0, 10, 223);
    }
}
