open module com.verntrax.nonogram.testutils {

    exports com.verntrax.nonogram.testutils;

    requires org.junit.jupiter.api;
    requires org.assertj.core;
}
