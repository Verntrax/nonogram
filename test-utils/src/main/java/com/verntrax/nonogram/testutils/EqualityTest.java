package com.verntrax.nonogram.testutils;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public interface EqualityTest<T> {

    @Test
    default void shouldBeEqualToItself() {
        // given
        T object = newTestedObject();

        // then
        assertThat(object).isEqualTo(object);
    }

    @Test
    default void shouldBeEqualToIdenticalObject() {
        // given
        T object = newTestedObject();
        T identicalObject = newTestedObject();

        // then
        assertThat(object).isEqualTo(identicalObject);
    }

    @Test
    default void shouldNotBeEqualToObjectOfDifferentClass() {
        // given
        T object = newTestedObject();

        // then
        assertThat(object).isNotEqualTo("totally different object");
    }

    @Test
    default void shouldNotBeEqualToDifferentObjects() {
        // given
        T object = newTestedObject();
        List<T> differentObjects = differentObjects();

        // then
        IntStream.range(0, differentObjects.size()).forEach(i ->
                SoftAssertions.assertSoftly(softly ->
                        softly.assertThat(object)
                                .as("test no %s", i)
                                .isNotEqualTo(differentObjects.get(i))));
    }

    @Test
    default void shouldHaveSameHashCodeAsIdenticalObject() {
        // given
        T object = newTestedObject();
        T identicalObject = newTestedObject();

        // then
        assertThat(object.hashCode()).isEqualTo(identicalObject.hashCode());
    }

    @Test
    default void shouldHaveOtherHashCodeThanDifferentObjects() {
        // given
        T object = newTestedObject();
        List<T> differentObjects = differentObjects();

        // then
        IntStream.range(0, differentObjects.size()).forEach(i ->
                SoftAssertions.assertSoftly(softly ->
                        softly.assertThat(object.hashCode())
                                .as("test no %s", i)
                                .isNotEqualTo(differentObjects.get(i).hashCode())));
    }

    T newTestedObject();

    List<T> differentObjects();
}
